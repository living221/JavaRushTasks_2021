package com.javarush.task.task10.task1012;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/* 
Количество букв
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        // Алфавит
        List<Character> alphabet = Arrays.asList(
                'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж',
                'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о',
                'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц',
                'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');

        // Ввод строк
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < 3; i++) {
            String line = reader.readLine();
            list.add(line.toLowerCase());
        }

        Map<Integer, Integer> collect = list.stream()
                .flatMapToInt(s -> s.chars())
                .boxed()
                .collect(Collectors.toMap(Function.identity(), integer -> 1, (t, t2) -> ++t));


        alphabet.forEach(character ->
                System.out.println(character + " " + collect.getOrDefault(((int)character), 0)));
        // напишите тут ваш код
//        for(String listItem : myArrayList){
//            if(myString.contains(listItem)){
//                // do something.
//            }
//        }


    }
}
