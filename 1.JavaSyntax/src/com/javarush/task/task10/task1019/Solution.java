package com.javarush.task.task10.task1019;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* 
Функциональности маловато!
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {
            while (true) {
                String id1 = reader.readLine();
                if (id1.equals("")) {
                    break;
                }
                int id = Integer.parseInt(id1);
                String name = reader.readLine();
                while (map.containsKey(name)) {
                    name = reader.readLine();
                }
                map.put(name, id);
            }

        }
        catch (Exception e){
            System.out.println("Error while entering lines.");
        }

        for (HashMap.Entry<String, Integer> pair : map.entrySet()) {
            int id = pair.getValue();
            String name = pair.getKey();
            System.out.println(id + " " + name);
        }
    }
}
