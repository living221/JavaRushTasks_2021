package com.javarush.task.task10.task1013;

/* 
Конструкторы класса Human
*/

public class Solution {
    public static void main(String[] args) {
    }

    public static class Human {
        // Напишите тут ваши переменные и конструкторы
        private String name;
        private Boolean sex;
        private int age;
        private String profession;
        private String race;
        private int salary;

        public Human() {
            name = "";
            sex = true;
            age = 0;
        }

        public Human(String name) {
            name = "";
            sex = true;
            age = 0;
        }
        public Human(Boolean sex) {
            name = "";
            sex = true;
            age = 0;
        }
        public Human(int age) {
            name = "";
            sex = true;
            age = 0;
        }
        public Human(String profession, int salary) {
            name = "";
            sex = true;
            age = 0;
        }
        public Human(int salary, String race) {
            name = "";
            sex = true;
            age = 0;
        }
        public Human(int salary, int age) {
            name = "";
            sex = true;
            age = 0;
        }
        public Human(int age, Boolean sex) {
            name = "";
            sex = true;
            age = 0;
        }
        public Human(Boolean sex, String name) {
            name = "";
            sex = true;
            age = 0;
        }
        public Human(String name, Boolean sex) {
            name = "";
            sex = true;
            age = 0;
        }

    }
}
