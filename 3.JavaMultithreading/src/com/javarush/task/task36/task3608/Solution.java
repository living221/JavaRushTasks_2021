package com.javarush.task.task36.task3608;

import com.javarush.task.task36.task3608.controller.Controller;
import com.javarush.task.task36.task3608.model.FakeModel;
import com.javarush.task.task36.task3608.model.MainModel;
import com.javarush.task.task36.task3608.model.Model;
import com.javarush.task.task36.task3608.view.EditUserView;
import com.javarush.task.task36.task3608.view.UsersView;

public class Solution {
    public static void main(String[] args) {
        Model model = new MainModel();
        UsersView usersView = new UsersView();
        Controller controller = new Controller();

        usersView.setController(controller);
        controller.setModel(model);
        controller.setUsersView(usersView);

        usersView.fireEventShowAllUsers();

        EditUserView editUserView = new EditUserView();
        controller.setEditUserView(editUserView);
        editUserView.setController(controller);

        usersView.fireEventOpenUserEditForm(126);

        editUserView.fireEventUserChanged("Pidor", 123, 3);

        editUserView.fireEventUserDeleted(124);

        usersView.fireEventShowDeletedUsers();


    }
}