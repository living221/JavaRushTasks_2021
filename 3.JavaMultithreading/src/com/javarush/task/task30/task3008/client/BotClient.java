package com.javarush.task.task30.task3008.client;

import com.javarush.task.task30.task3008.ConsoleHelper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class BotClient extends Client {

    @Override
    protected SocketThread getSocketThread() {
        return new BotSocketThread();
    }

    @Override
    protected boolean shouldSendTextFromConsole() {
        return false;
    }

    @Override
    protected String getUserName() {
        int seed = (int) (Math.random() * 100);
        String botName = "date_bot_" + seed;
        return botName;
    }

    public static void main(String[] args) {
        BotClient bot = new BotClient();
        bot.run();
    }

    public class BotSocketThread extends SocketThread {

        @Override
        protected void clientMainLoop() throws IOException, ClassNotFoundException {
            sendTextMessage("Привет чатику. Я бот. Понимаю команды: дата, день, месяц, год, время, час, минуты, секунды.");
            super.clientMainLoop();
        }

        @Override
        protected void processIncomingMessage(String message) {
            ConsoleHelper.writeMessage(message);
            if (!message.contains(": ")) return;
            
            String[] words = message.split(": ");
            String senderName = words[0];


            if (words[1].equals("дата")) {
                String pattern = "d.MM.YYYY";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                String date = simpleDateFormat.format(new Date().getTime());
                sendTextMessage("Информация для " + senderName + ": " + date);
            } else if (words[1].equals("день")) {
                String pattern = "d";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                String date = simpleDateFormat.format(new Date().getTime());
                sendTextMessage("Информация для " + senderName + ": " + date);
            } else if (words[1].equals("месяц")) {
                String pattern = "MMMM";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                String date = simpleDateFormat.format(new Date().getTime());
                sendTextMessage("Информация для " + senderName + ": " + date);
            } else if (words[1].equals("год")) {
                String pattern = "YYYY";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                String date = simpleDateFormat.format(new Date().getTime());
                sendTextMessage("Информация для " + senderName + ": " + date);
            } else if (words[1].equals("время")) {
                String pattern = "H:mm:ss";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                String date = simpleDateFormat.format(new Date().getTime());
                sendTextMessage("Информация для " + senderName + ": " + date);
            } else if (words[1].equals("час")) {
                String pattern = "H";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                String date = simpleDateFormat.format(new Date().getTime());
                sendTextMessage("Информация для " + senderName + ": " + date);
            } else if (words[1].equals("минуты")) {
                String pattern = "m";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                String date = simpleDateFormat.format(new Date().getTime());
                sendTextMessage("Информация для " + senderName + ": " + date);
            } else if (words[1].equals("секунды")) {
                String pattern = "s";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                String date = simpleDateFormat.format(new Date().getTime());
                sendTextMessage("Информация для " + senderName + ": " + date);
            }
        }
    }
}
