package com.javarush.task.task30.task3012;

/* 
Получи заданное число
*/

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.createExpression(74);
    }

    public void createExpression(int number) {
        //напишите тут ваш код
        StringBuilder sb = new StringBuilder(number + " =");
        int powCounter = 0;
        do {
            int remain = number % 3;
            number /= 3;
            if (remain == 1) {
                sb.append(" + " + (int) Math.pow(3, powCounter));
            }
            if (remain == 2) {
                sb.append(" - " + (int) Math.pow(3, powCounter));
                number++;
            }
            powCounter++;
        } while (number > 0);
        System.out.println(sb.toString());
    }
}