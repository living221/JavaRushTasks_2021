package com.javarush.task.task30.task3010;

import java.math.BigInteger;
import java.util.Locale;
import java.util.regex.Pattern;

/* 
Минимальное допустимое основание системы счисления
*/

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        String result = "incorrect";
        for (int i = 2; i <= 36; i++) {
            try {
                BigInteger number = new BigInteger(args[0], i);
                result = Integer.toString(i);
                break;
            } catch (Exception e) {
//                System.out.println("can't convert to radix: " + i);
            }

        }
        System.out.println(result);
    }
}