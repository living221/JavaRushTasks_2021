package com.javarush.task.task30.task3002;

/* 
Осваиваем методы класса Integer
*/

public class Solution {

    public static void main(String[] args) {
        System.out.println(convertToDecimalSystem("0x16")); //22
        System.out.println(convertToDecimalSystem("012"));  //10
        System.out.println(convertToDecimalSystem("0b10")); //2
        System.out.println(convertToDecimalSystem("62"));   //62
    }

    public static String convertToDecimalSystem(String s) {
        //напишите тут ваш код
        String result = s;
        int radix = 10;
        if (s.contains("x")) {
            result = s.substring(2);
            radix = 16;
        } else if (s.contains("b")) {
            result = s.substring(2);
            radix = 2;
        } else if (s.startsWith("0")) {
            //result = s.substring(1);
            radix = 8;
        }

        int res = Integer.parseInt(result, radix);
        return Integer.toString(res);
    }
}
