package com.javarush.task.task26.task2601;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/* 
Почитать в инете про медиану выборки
*/

public class Solution {

    public static void main(String[] args) {
        Integer[] testArray0 = {13, 8, 15, 5, 18, 11};
        Integer[] testArray1 = {10, 20, 20, 30, 20, 50, 60, 22};
        Integer[] testArray2 = {10, 8, 12, 6, 19};
        Integer[] testArray3 = {11, 3, 122, 36, 18, 0, 1};


//        double median0 = median(testArray0);
//        System.out.println(median0);
//        double median1 = median(testArray1);
//        System.out.println(median1);
//        double median2 = median(testArray2);
//        System.out.println(median2);
//        double median3 = median(testArray3);
//        System.out.println(median3);

//        System.out.println(Arrays.toString(sort(testArray0)));
//        System.out.println(Arrays.toString(sort(testArray1)));
//        System.out.println(Arrays.toString(sort(testArray2)));
//        System.out.println(Arrays.toString(sort(testArray3)));
    }

    public static Integer[] sort(Integer[] array) {
        //implement logic here
        double median = median(array);

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(array));
        Collections.sort(list);

        Comparator<Integer> compareByMedian = new Comparator<Integer>() {

            @Override
            public int compare(Integer o1, Integer o2) {

                return Math.abs((int) median - o1) - Math.abs((int) median - o2);
            }
        };

        list.sort(compareByMedian);
        array = new Integer[list.size()];
        return list.toArray(array);
    }

    private static double median(Integer[] array) {
        Arrays.sort(array);
        double median;
        if (array.length % 2 == 0)
            median = ((double) array[array.length / 2] + (double) array[array.length / 2 - 1]) / 2;
        else
            median = (double) array[array.length / 2];
        return median;
    }
}
