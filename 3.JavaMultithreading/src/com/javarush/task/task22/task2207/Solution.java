package com.javarush.task.task22.task2207;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/* 
Обращенные слова

валидатор дятел
выпил пива
а дятел ротадилав
еще пива липыв
а валидатор всерамно летяд
азза еще

[валидатор ротадилав, дятел летяд, выпил липыв, а а, еще еще]
*/

public class Solution {
    public static List<Pair> result = new LinkedList<>();

    public static void main(String[] args) {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileName = reader.readLine();

            try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {

                String st;
                //strings contains all words separated by " " from file
                List<String> strings = new LinkedList<>();

                while ((st = br.readLine()) != null) {
                    String[] stringsArr = st.split(" ");
                    strings.addAll(Arrays.asList(stringsArr));
                }

                //Checking if exist a pair of word and reversed word.
                // If pair exists words in strings list swapped to "".
                for (int k = 0; k < Objects.requireNonNull(strings).size(); k++) {
                    for (int i = k + 1; i < strings.size(); i++) {
                        StringBuilder tmpStringBuild = new StringBuilder(strings.get(i));
                        tmpStringBuild.reverse();
                        String reversedString = tmpStringBuild.toString();
                        if (strings.get(k).equals(reversedString) && !(strings.get(k).equals(""))) {
                            result.add(new Pair(strings.get(k), strings.get(i)));
                            strings.set(i, "");
                            strings.set(k, "");
                        }
                    }
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(result);
    }

    public static class Pair {
        String first;
        String second;

        public Pair() {
        }

        public Pair(String first, String second) {
            this.first = first;
            this.second = second;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pair pair = (Pair) o;

            if (first != null ? !first.equals(pair.first) : pair.first != null) return false;
            return second != null ? second.equals(pair.second) : pair.second == null;

        }

        @Override
        public int hashCode() {
            int result = first != null ? first.hashCode() : 0;
            result = 31 * result + (second != null ? second.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return first == null && second == null ? "" :
                    first == null ? second :
                            second == null ? first :
                                    first.compareTo(second) < 0 ? first + " " + second : second + " " + first;

        }
    }

}
