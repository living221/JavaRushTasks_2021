package com.javarush.task.task22.task2211;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/* 
Смена кодировки
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        try (FileInputStream fis = new FileInputStream(args[0]);
             FileOutputStream fos = new FileOutputStream(args[1])) {
            Charset win1251 = Charset.forName("Windows-1251");
            byte[] data = new byte[fis.available()];
            //fis.read(data);
            String utfString = new String(data, win1251);
            data = utfString.getBytes(StandardCharsets.UTF_8);

            fos.write(data);
        }
    }
}
