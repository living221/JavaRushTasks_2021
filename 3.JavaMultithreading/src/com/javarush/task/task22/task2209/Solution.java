package com.javarush.task.task22.task2209;

import java.io.*;
import java.util.*;

/* 
Составить цепочку слов
*/

public class Solution {
    public static void main(String[] args) {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileName = reader.readLine();

            try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {

                String st;
                String[] stringsArr = new String[0];

                while ((st = br.readLine()) != null) {
                    stringsArr = st.split(" ");
                }

                //System.out.println(Arrays.toString(stringsArr));
                long startTime = System.nanoTime() / 1_000;
                StringBuilder result = getLine(stringsArr);
                long endTime   = System.nanoTime() / 1_000;
                long totalTime = endTime - startTime;
                System.out.println(totalTime);
                System.out.println(result.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //...

    }

    public static StringBuilder getLine(String... words) {

        if (words == null || words.length == 0) {
            return new StringBuilder();
        }
        StringBuilder result = new StringBuilder();
        StringBuilder tmpResult = new StringBuilder();
        String res = null;

        int maxUnusedWords = words.length;


        for (String s : words) {
            tmpResult.append(s);

            ArrayList<String> tmpWords = new ArrayList<>(Arrays.asList(words));
            tmpWords.remove(s);

            int badInsertAttempts = 0;
            int chainLength = 0;

            while (!tmpWords.isEmpty()) {
                //without shuffle some cases won't get longest chain.
                Collections.shuffle(tmpWords);

                if (tmpWords.get(0).substring(0, 1).equalsIgnoreCase(tmpResult.substring(tmpResult.length() - 1))) {
                    tmpResult.append(" ");
                    tmpResult.append(tmpWords.get(0));

                    tmpWords.remove(0);
                } else if (tmpWords.get(0).substring(tmpWords.get(0).length() - 1).equalsIgnoreCase(tmpResult.substring(0, 1))) {
                    tmpResult.insert(0, tmpWords.get(0) + " ");
                    tmpWords.remove(0);
                } else {
                    String tmp = tmpWords.get(0);
                    tmpWords.remove(0);
                    tmpWords.add(tmp);
                    badInsertAttempts++;
                }
                //badInsertAttempts - should be optimized.
                if (badInsertAttempts > 10_000) {

                    chainLength = tmpWords.size();

                    for (String notUsedString : tmpWords) {
                        tmpResult.append(" ");
                        tmpResult.append(notUsedString);
                    }
                    tmpWords.clear();
                }
            }
            if (chainLength < maxUnusedWords) {
                maxUnusedWords = chainLength;
                res = tmpResult.toString();
            }
            tmpResult.setLength(0);
        }

        result.append(res);
        return result;
    }
}
