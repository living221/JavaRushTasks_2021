package com.javarush.task.task22.task2208;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/* 
Формируем WHERE
*/

public class Solution {
    public static void main(String[] args) {

        Map<String, String> testmap = new HashMap<>();
        /*testmap.put("name", "Ivanov");
        testmap.put("country", "Ukraine");
        testmap.put("city", "Kiev");
        testmap.put("age", null);*/
        testmap.put("name", null);
        testmap.put("country", null);
        testmap.put("city", null);
        testmap.put("age", null);
        System.out.println(testmap);

        System.out.println(getQuery(testmap));
    }

    public static String getQuery(Map<String, String> params) {

        StringBuilder query = new StringBuilder();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (entry.getValue() != null) {

                query.append(entry.getKey());
                query.append(" = '");
                query.append(entry.getValue());
                query.append("' and ");
            }
        }
        //trim last "' and ".
        if (query.length() > 5){
            query.setLength(query.length() - 5);
        }
        return query.toString();
    }
}
