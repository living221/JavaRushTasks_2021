package com.javarush.task.task22.task2212;

/* 
Проверка номера телефона
*/

public class Solution {
    public static boolean checkTelNumber(String telNumber) {

        if( telNumber==null || telNumber.length() < 10) return false;

        String pattern = "(((?=\\+.{14}$)|(?=\\+\\d{12}$))^\\+\\d*(\\(\\d{3}\\))*\\d+$)|(((?=.{12}$)|(?=\\d{10}$))^\\d*(\\(\\d{3}\\))*\\d+$)";
        return telNumber.matches(pattern);
    }

    public static void main(String[] args) {
        String number1 = "+380501234567"; //true
        String number2 = "+38(050)1234567"; //true
        String number3 = "(050)1234567"; //true
        String number4 = "0(501)234567"; //true
        String number5 = "+38)050(1234567"; //false
        String number6 = "+38(050)123-45-67"; //false
        String number7 = "050ххх4567"; //false
        String number8 = "050123456"; //false
        String number9 = "(0)501234567"; //false

        System.out.println(checkTelNumber(number1));
        System.out.println(checkTelNumber(number2));
        System.out.println(checkTelNumber(number3));
        System.out.println(checkTelNumber(number4));
        System.out.println(checkTelNumber(number5));
        System.out.println(checkTelNumber(number6));
        System.out.println(checkTelNumber(number7));
        System.out.println(checkTelNumber(number8));
        System.out.println(checkTelNumber(number9));

    }
}
