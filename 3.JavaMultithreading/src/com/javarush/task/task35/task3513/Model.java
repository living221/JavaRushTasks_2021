package com.javarush.task.task35.task3513;

import java.util.*;

public class Model {
    private static final int FIELD_WIDTH = 4;
    private Tile[][] gameTiles = new Tile[FIELD_WIDTH][FIELD_WIDTH];
    public int score;
    public int maxTile;
    private Stack<Tile[][]> previousStates = new Stack<>();
    private Stack<Integer> previousScores = new Stack<>();
    private boolean isSaveNeeded = true;


    public Model() {
        resetGameTiles();
    }

    public void resetGameTiles() {
        for (int i = 0; i < this.gameTiles.length; i++) {
            for (int j = 0; j < this.gameTiles[i].length; j++) {
                this.gameTiles[i][j] = new Tile();
            }
        }
        addTile();
        addTile();
        this.score = 0;
        this.maxTile = 0;
    }

    public Tile[][] getGameTiles() {
        return gameTiles;
    }

    private void addTile() {
        if (!getEmptyTiles().isEmpty()) {
            Tile tile = getEmptyTiles().get((int) (getEmptyTiles().size() * Math.random()));
            tile.value = (Math.random() < 0.9 ? 2 : 4);
        }

    }

    private List<Tile> getEmptyTiles() {
        List<Tile> emptyTiles = new ArrayList<>();
        for (Tile[] tileAr : gameTiles) {
            for (Tile tile : tileAr) {
                if (tile.isEmpty()) {
                    emptyTiles.add(tile);
                }
            }
        }
        return emptyTiles;
    }

    private boolean compressTiles(Tile[] tiles) {
        boolean isChanged = false;
        int insertPosition = 0;
        for (int i = 0; i < FIELD_WIDTH; i++) {
            if (!tiles[i].isEmpty()) {
                if (i != insertPosition) {
                    tiles[insertPosition] = tiles[i];
                    tiles[i] = new Tile();
                    isChanged = true;
                }
                insertPosition++;
            }
        }
        return isChanged;
    }

    private boolean mergeTiles(Tile[] tiles) {
        boolean isChanged = false;
        LinkedList<Tile> tilesList = new LinkedList<>();
        for (int i = 0; i < FIELD_WIDTH; i++) {
            if (tiles[i].isEmpty()) {
                continue;
            }

            if (i < FIELD_WIDTH - 1 && tiles[i].value == tiles[i + 1].value) {
                int updatedValue = tiles[i].value * 2;
                if (updatedValue > maxTile) {
                    maxTile = updatedValue;
                }
                score += updatedValue;
                tilesList.addLast(new Tile(updatedValue));
                tiles[i + 1].value = 0;
                isChanged = true;
            } else {
                tilesList.addLast(new Tile(tiles[i].value));
            }
            tiles[i].value = 0;
        }

        for (int i = 0; i < tilesList.size(); i++) {
            tiles[i] = tilesList.get(i);
        }
        return isChanged;
    }

    public boolean canMove() {
        boolean result = false;
        if (getEmptyTiles().size() > 0) {
            return true;
        }
        for (int i = 0; i < FIELD_WIDTH - 1; i++) {
            for (int j = 0; j < FIELD_WIDTH - 1; j++) {
                if (gameTiles[i][j].value == gameTiles[i][j + 1].value || (gameTiles[i][j].value == gameTiles[i + 1][j].value)) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    public Tile[][] rotationClockwise(Tile[][] tiles) {
        Tile[][] result = new Tile[FIELD_WIDTH][FIELD_WIDTH];
        for (int i = 0; i < FIELD_WIDTH; i++) {
            for (int j = 0; j < FIELD_WIDTH; j++) {
                result[i][j] = tiles[FIELD_WIDTH - 1 - j][i];
            }
        }
        return result;
    }

    public void left() {
        if (isSaveNeeded) {
            saveState(gameTiles);
        }
        boolean isChanged = false;
        for (Tile[] tileAr : gameTiles) {
            if (compressTiles(tileAr) | mergeTiles(tileAr)) {
                isChanged = true;
            }
        }
        if (isChanged) addTile();
        isSaveNeeded = true;
    }

    public void right() {
        saveState(gameTiles);
        gameTiles = rotationClockwise(gameTiles);
        gameTiles = rotationClockwise(gameTiles);
        left();
        gameTiles = rotationClockwise(gameTiles);
        gameTiles = rotationClockwise(gameTiles);
    }

    public void up() {
        saveState(gameTiles);
        gameTiles = rotationClockwise(gameTiles);
        gameTiles = rotationClockwise(gameTiles);
        gameTiles = rotationClockwise(gameTiles);
        left();
        gameTiles = rotationClockwise(gameTiles);
    }

    public void down() {
        saveState(gameTiles);
        gameTiles = rotationClockwise(gameTiles);
        left();
        gameTiles = rotationClockwise(gameTiles);
        gameTiles = rotationClockwise(gameTiles);
        gameTiles = rotationClockwise(gameTiles);
    }

    private void saveState(Tile[][] gameTiles) {
        Tile[][] saveGame = new Tile[FIELD_WIDTH][FIELD_WIDTH];
        for (int i = 0; i < FIELD_WIDTH; i++) {
            for (int j = 0; j < FIELD_WIDTH; j++) {
                saveGame[i][j] = new Tile(gameTiles[i][j].value);
            }
        }
        previousStates.push(saveGame);
        previousScores.push(score);
        isSaveNeeded = false;
    }

    public void rollback() {
        if (!previousStates.isEmpty() && !previousScores.isEmpty()) {
            gameTiles = previousStates.pop();
            score = previousScores.pop();
        }
    }

    public void randomMove() {
        int n = ((int) (Math.random() * 100)) % 4;
        if (n == 0) {
            left();
        } else if (n == 1) {
            right();
        } else if (n == 2) {
            up();
        } else if (n == 3) {
            down();
        }
    }

    public boolean hasBoardChanged() {
        boolean result = false;
        Tile[][] tempTiles = previousStates.peek();

        for (int i = 0; i < FIELD_WIDTH; i++) {
            for (int j = 0; j < FIELD_WIDTH; j++) {
                if (tempTiles[i][j].value != gameTiles[i][j].value) {
                    result = true;
                }
            }
        }
        return result;
    }

    private MoveEfficiency getMoveEfficiency(Move move) {
        move.move();
        MoveEfficiency moveEfficiency = new MoveEfficiency(getEmptyTiles().size(), score, move);
        if (!hasBoardChanged()) {
            return new MoveEfficiency(-1, 0, move);
        }
        rollback();
        return moveEfficiency;
    }

    public void autoMove() {
        PriorityQueue<MoveEfficiency> queue = new PriorityQueue<>(4, Collections.reverseOrder());
        queue.add(getMoveEfficiency(new Move() {
            @Override
            public void move() {
                left();
            }
        }));
        queue.add(getMoveEfficiency(new Move() {
            @Override
            public void move() {
                right();
            }
        }));
        queue.add(getMoveEfficiency(new Move() {
            @Override
            public void move() {
                up();
            }
        }));
        queue.add(getMoveEfficiency(new Move() {
            @Override
            public void move() {
                down();
            }
        }));
        queue.peek().getMove().move();
    }
}
