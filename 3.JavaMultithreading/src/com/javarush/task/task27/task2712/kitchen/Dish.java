package com.javarush.task.task27.task2712.kitchen;

public enum Dish {
    FISH(25),
    STEAK(30),
    SOUP(15),
    JUICE(5),
    WATER(3);

    private int duration;

    public int getDuration() {
        return duration;
    }

    Dish(int duration) {
        this.duration = duration;
    }

    public static String allDishesToString() {
        StringBuilder result = new StringBuilder();
        Dish[] dishes = Dish.values();
        for (Dish dish : dishes) {
            result.append(dish.toString()).append(", ");
        }
        return result.substring(0,result.length()-2);
    }
}
