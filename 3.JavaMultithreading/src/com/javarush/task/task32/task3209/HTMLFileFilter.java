package com.javarush.task.task32.task3209;

import javax.swing.filechooser.FileFilter;
import java.io.File;
import java.util.Locale;

public class HTMLFileFilter extends FileFilter {
    @Override
    public boolean accept(File file) {

        String filename = file.getName().toLowerCase(Locale.ROOT);

            return filename.endsWith(".htm") || filename.endsWith(".html") || file.isDirectory();


    }

    @Override
    public String getDescription() {
        return "HTML и HTM файлы";
    }
}
