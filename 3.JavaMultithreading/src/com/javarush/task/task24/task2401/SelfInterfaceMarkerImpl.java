package com.javarush.task.task24.task2401;

public class SelfInterfaceMarkerImpl implements SelfInterfaceMarker {

    public void foo () {
        System.out.println("foo");
    }

    public void bar () {
        System.out.println("bar");
    }
}
