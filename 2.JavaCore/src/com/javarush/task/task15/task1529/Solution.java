package com.javarush.task.task15.task1529;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Осваивание статического блока
*/

public class Solution {
    public static void main(String[] args) {

    }

    static {
        //add your code here - добавьте код тут
        reset();
    }

    public static CanFly result;

    public static void reset() {
        //add your code here - добавьте код тут
        String choice;
        int passengersCount;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            choice = reader.readLine();


            if (choice.equals("helicopter")){
                result = new Helicopter();
            }
            else if (choice.equals("plane")){
                passengersCount = Integer.parseInt(reader.readLine());
                result = new Plane(passengersCount);
            }
            else {
                reader.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
