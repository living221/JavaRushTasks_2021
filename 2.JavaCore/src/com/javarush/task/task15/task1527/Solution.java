package com.javarush.task.task15.task1527;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
Парсер реквестов
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String url = reader.readLine();
        //напишите тут ваш код
        String[] list = url.split("\\?")[1].split("&");

        StringBuilder resultBuilder = new StringBuilder();

        for (String subSt : list) {
            if (subSt.contains("=")) {
                resultBuilder.append(subSt.substring(0, subSt.indexOf("=")) + " ");
            } else if (!(subSt.contains("="))) {
                resultBuilder.append(subSt + " ");
            }
        }

        System.out.println(resultBuilder.toString().trim());

        for (String subObj : list) {
            if (subObj.substring(0, 4).contains("obj=")) {
                try {
                    alert(Double.parseDouble(subObj.substring(4)));
                } catch (NumberFormatException e) {
                    alert(subObj.substring(4));
                }
            }
        }
    }

    public static void alert(double value) {
        System.out.println("double: " + value);
    }

    public static void alert(String value) {
        System.out.println("String: " + value);
    }
}