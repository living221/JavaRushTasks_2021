package com.javarush.task.task15.task1523;

/* 
Перегрузка конструкторов
*/

public class Solution {
    String a = null;
    String b = null;
    String c = null;

    Solution () {

    }

    protected Solution (String a) {
        a = this.a;
    }

    private Solution (String b, String a) {
        b = this.b;
        a = this.a;
    }

    public Solution (String a, String b, String c) {
        a = this.a;
        b = this.b;
        c = this.c;
    }



    public static void main(String[] args) {

    }
}

