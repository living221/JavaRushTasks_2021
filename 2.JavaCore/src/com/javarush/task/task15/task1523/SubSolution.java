package com.javarush.task.task15.task1523;

public class SubSolution extends Solution{
    protected SubSolution(String a) {
        super(a);
    }

    public SubSolution(String a, String b, String c) {
        super(a, b, c);
    }

    SubSolution() {
        super();
    }
}
