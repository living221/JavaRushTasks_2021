package com.javarush.task.task15.task1519;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* 
Разные методы для разных типов
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        //напиште тут ваш код

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String line;

        while (true){
            line = reader.readLine();

            if (line.equals("exit")){
                break;
            }
            if (line.contains(".")&&isNumeric(line)){
                print(Double.parseDouble(line));
                System.out.println("double correct use");
            }
            else if ((!line.contains("."))&&isNumeric(line)){
                if((Double.parseDouble(line))>0&&(Double.parseDouble(line))<128){
                    print(Short.parseShort(line));
                    System.out.println("short correct use");
                }
                else if ((Double.parseDouble(line))<=0||(Double.parseDouble(line))>=128){
                    print(Integer.parseInt(line));
                    System.out.println("integer correct use");
                }
            }
            else {
                print(line);
                System.out.println("else use");
            }
        }
        reader.close();
    }

    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

    public static void print(Double value) {
        System.out.println("Это тип Double, значение " + value);
    }

    public static void print(String value) { System.out.println("Это тип String, значение " + value); }

    public static void print(short value) {
        System.out.println("Это тип short, значение " + value);
    }

    public static void print(Integer value) {
        System.out.println("Это тип Integer, значение " + value);
    }
}
