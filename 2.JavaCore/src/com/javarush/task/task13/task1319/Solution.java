package com.javarush.task.task13.task1319;

import java.io.*;
import java.nio.file.InvalidPathException;
import java.util.ArrayList;
import java.util.List;
import java.nio.file.Paths;

/* 
Писатель в файл с консоли
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        // напишите тут ваш код
        List<String> bufferText = new ArrayList<String>();

        BufferedReader readerText = new BufferedReader(new InputStreamReader(System.in));
        String outputFileName;
        outputFileName = readerText.readLine();


        if (isPathValid(outputFileName) == true ) {
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFileName));
            String line;

            while (true) {
                line = readerText.readLine();
                writer.write(line + "\n");
                if (line.equals("exit")) {
                    break;
                }
            }
            writer.close();
        }

        readerText.close();
    }

    public static boolean isPathValid(String path) {

        try {

            Paths.get(path);

        } catch (InvalidPathException ex) {
            System.out.println("incorrect file name");
            return false;
        }

        return true;
    }
}
