package com.javarush.task.task13.task1318;

import java.io.*;

/* 
Чтение файла
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        // напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filePath = reader.readLine();

        File f = new File(filePath);
        if(f.exists() && !f.isDirectory()) {
            InputStream inStream = new FileInputStream(filePath);
            BufferedReader fileBuffer = new BufferedReader(new InputStreamReader(inStream));


            while (fileBuffer.ready())
            {
                System.out.println(fileBuffer.readLine());
            }
            inStream.close();
        }
        else System.out.println("Can't find such file.");


        reader.close();



    }
}