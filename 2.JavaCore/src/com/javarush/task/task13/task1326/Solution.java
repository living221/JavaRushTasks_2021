package com.javarush.task.task13.task1326;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* 
Сортировка четных чисел из файла
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        // напишите тут ваш код
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileName = reader.readLine();

            File f = new File(fileName);

            if (f.exists() && !f.isDirectory()) {

                InputStream inStream = new FileInputStream(fileName);
                BufferedReader fileBuffer = new BufferedReader(new InputStreamReader(inStream));

                List<Integer> data = new ArrayList<>();

                while (fileBuffer.ready()) {
                    int tmp = Integer.parseInt(fileBuffer.readLine());
                    if (tmp % 2 == 0) {
                        data.add(tmp);
                    }
                }
                inStream.close();
                data.stream()
                        .sorted()
                        .forEach(System.out::println);

            } else {
                System.out.println("Can't find such file.");
            }
        }
    }
}


