package com.javarush.task.task19.task1919;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

/* 
Считаем зарплаты
*/

public class Solution {
    public static void main(String[] args) throws IOException {

        ArrayList<String> fileContent1 = new ArrayList<>();

        try(BufferedReader br = new BufferedReader(new FileReader(args[0]))){

            while (br.ready()) {
                fileContent1.add(br.readLine());
            }

            TreeMap <String, Double> salaries = new TreeMap<>();

            String [] tmp;

            for (String s :
                    fileContent1) {
                tmp = s.split(" ");
                salaries.merge(tmp[0], Double.parseDouble(tmp[1]), Double::sum);
            }
            salaries.forEach((key, value) -> System.out.println(key + " " + value));
        }
    }
}
