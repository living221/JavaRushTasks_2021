package com.javarush.task.task19.task1925;

import java.io.*;
import java.util.ArrayList;

/* 
Длинные слова
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(args[0]))) {
            ArrayList<String> tmp = new ArrayList<>();
            while (br.ready()) {
                String[] fileContent = br.readLine().split(" ");
                for (String s :
                        fileContent) {
                    if (s.length() > 6) {
                        tmp.add(s);
                    }
                }
            }
            try (FileWriter fw = new FileWriter(args[1])) {
                fw.write(String.join(",", tmp));
            }
        }
    }
}
