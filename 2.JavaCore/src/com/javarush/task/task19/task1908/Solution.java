package com.javarush.task.task19.task1908;

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Pattern;

/*
Выделяем числа
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileNameFrom = reader.readLine();
            String fileNameTo = reader.readLine();

            try (BufferedReader br1 = new BufferedReader(new FileReader(fileNameFrom));
                 BufferedWriter bw2 = new BufferedWriter(new FileWriter(fileNameTo))) {
                String[] splitted = null;
                while (br1.ready()) {
                    splitted = br1.readLine().split("\\s+");
                }
                for (String s :
                        splitted) {
                    if (Pattern.matches("[0-9]+", s)) {
                        bw2.write(s);
                        bw2.write(" ");
                    }
                }
            }
        }
    }
}
