package com.javarush.task.task19.task1902;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/* 
Адаптер
*/

public class AdapterFileOutputStream implements AmigoStringWriter {

    public static void main(String[] args) {

    }

    private FileOutputStream fileOutputStream;

    AdapterFileOutputStream(FileOutputStream fileOutputStream)
    {
        this.fileOutputStream = fileOutputStream;
    }


    @Override
    public void flush() throws IOException {
        this.fileOutputStream.flush();
    }

    @Override
    public void writeString(String s) throws IOException {
        byte[] bytesFromString =s.getBytes(StandardCharsets.UTF_8); //converting string into byte array
        this.fileOutputStream.write(bytesFromString);
    }

    @Override
    public void close() throws IOException {
        this.fileOutputStream.close();
    }
}

