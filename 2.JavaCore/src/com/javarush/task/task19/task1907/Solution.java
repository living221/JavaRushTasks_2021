package com.javarush.task.task19.task1907;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* 
Считаем слово
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileName = reader.readLine();

            FileReader readerFF = new FileReader(fileName);
            Scanner scan = new Scanner(readerFF);

            ArrayList<Integer> fileContent = new ArrayList<>();
            int worldCounter = 0;
            String tmp;

            while (scan.hasNextLine()) {
                tmp = scan.nextLine();

                Pattern p = Pattern.compile("world");
                Matcher m = p.matcher(tmp);

                if (m.find()) {
                    worldCounter++;
                }
            }
            readerFF.close();
            System.out.println(worldCounter);
        }
    }
}
