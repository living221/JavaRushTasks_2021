package com.javarush.task.task19.task1921;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/* 
Хуан Хуанович
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) throws IOException, ParseException {

        try(BufferedReader br = new BufferedReader(new FileReader(args[0]))) {

            while (br.ready()) {
                String[] fileContent = br.readLine().split(" ");
                StringJoiner name = new StringJoiner(" ");
                StringJoiner date = new StringJoiner("/");


                for (int i = 0; i < fileContent.length - 3; i++) name.add(fileContent[i]);
                for (int i = fileContent.length - 3; i < fileContent.length; i++) date.add(fileContent[i]);

                PEOPLE.add(new Person(name.toString(), new SimpleDateFormat("dd/MM/yyyy").parse(date.toString())));
            }
            System.out.println(PEOPLE);
        }
    }
}
