package com.javarush.task.task19.task1923;

import java.io.*;
import java.util.ArrayList;

/* 
Слова с цифрами
*/

public class Solution {
    public static void main(String[] args) throws IOException {

        try (BufferedReader br = new BufferedReader(new FileReader(args[0]))) {
            ArrayList<String> tmp = new ArrayList<>();
            while (br.ready()) {
                String[] fileContent = br.readLine().split(" ");
                for (String s :
                        fileContent) {
                    if (containsDigit(s)) {
                        tmp.add(s + " ");
                    }
                }
            }
            try (FileWriter fw = new FileWriter(args[1])) {
                for (String s :
                        tmp) {
                    fw.write(s);
                }
            }
        }
    }

    public static final boolean containsDigit(String s) {
        boolean containsDigit = false;

        if (s != null && !s.isEmpty()) {
            for (char c : s.toCharArray()) {
                if (containsDigit = Character.isDigit(c)) {
                    break;
                }
            }
        }

        return containsDigit;
    }
}
