package com.javarush.task.task19.task1922;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* 
Ищем нужные строки
*/

public class Solution {
    public static List<String> words = new ArrayList<String>();

    static {
        words.add("файл");
        words.add("вид");
        words.add("В");
    }

    public static void main(String[] args) throws IOException {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileName = reader.readLine();

            try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
                while (br.ready()) {
                    String[] fileContent = br.readLine().split(" ");
                    int refCount = 0;
                    for (String s :
                            fileContent) {
                        for (String word : words) {
                            if (s.equals(word)) {
                                refCount++;
                            }
                        }
                    }
                    if (refCount == 2){
                        for (String s :
                                fileContent) {
                            System.out.print(s + " ") ;

                        }
                        System.out.println();
                    }
                }
            }
        }
    }
}
