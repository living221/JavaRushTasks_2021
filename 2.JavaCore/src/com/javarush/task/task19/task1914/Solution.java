package com.javarush.task.task19.task1914;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

/* 
Решаем пример
*/

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream consoleStream = System.out;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        PrintStream stream = new PrintStream(outputStream);

        System.setOut(stream);

        testString.printSomething();

        String result = outputStream.toString();

        System.setOut(consoleStream);

        Expression(result);
    }

    private static void Expression(String result) {
        String[] buff = result.split(" ");
        ArrayList<Integer> variables = new ArrayList<>();

        for (String s :
                buff) {
            if (s.matches("[\\d]+")) {
                variables.add(Integer.parseInt(s));
            }
        }

        if (result.contains("+")) {
            int sum = variables.get(0) + variables.get(1);
            System.out.println(result.trim() + " " + sum);
        }

        if (result.contains("-")) {
            int sub = variables.get(0) - variables.get(1);
            System.out.println(result.trim() + " " + sub);
        }

        if (result.contains("*")) {
            int multi = variables.get(0) * variables.get(1);
            System.out.println(result.trim() + " " + multi);
        }
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("3 + 6 = ");
        }
    }
}

