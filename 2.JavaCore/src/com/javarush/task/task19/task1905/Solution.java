package com.javarush.task.task19.task1905;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/* 
Закрепляем адаптер
*/

public class Solution {
    public static Map<String, String> countries = new HashMap<String, String>();

    public static void main(String[] args) {
        /*testing*/
        Contact contact = new Person();
        Customer customer = new Person();
        RowItem rowItem = new DataAdapter(customer, contact);
        System.out.println(rowItem.getContactFirstName());
        System.out.println(rowItem.getContactLastName());
        System.out.println(rowItem.getCompany());
        System.out.println(rowItem.getCountryCode());
        System.out.println(rowItem.getDialString());
        /*testing*/
    }
/*testing*/
    public static class Person implements Contact, Customer {
    @Override
    public String getName() {
        return "Ivanov, Ivan";
    }

    @Override
    public String getPhoneNumber() {
        return "+38(050)123-45-67";
    }

    @Override
    public String getCompanyName() {
        return "JavaRush Ltd.";
    }

    @Override
    public String getCountryName() {
        return "Ukraine";
    }
}
/*testing*/

    static {
        countries.put("UA", "Ukraine");
        countries.put("RU", "Russia");
        countries.put("CA", "Canada");

    }

    public static class DataAdapter implements RowItem {

        private Customer customer;

        private Contact contact;

        public DataAdapter(Customer customer, Contact contact) {
            this.customer = customer;
            this.contact = contact;
        }



        @Override
        public String getCountryCode() {
            String result = null;
            for(Map.Entry<String, String> entry: countries.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (value.equals(this.customer.getCountryName())) {
                    result = key;
                }
            }
            return result;
        }

        @Override
        public String getCompany() {
            return this.customer.getCompanyName();
        }

        @Override
        public String getContactFirstName() {
            String fullName = this.contact.getName();
            String [] separatedName = fullName.split(", ");
            return separatedName[1];
        }

        @Override
        public String getContactLastName() {
            String fullName = this.contact.getName();
            String [] separatedName = fullName.split(",");
            return separatedName[0];
        }

        @Override
        public String getDialString() {
            String phone = this.contact.getPhoneNumber();
            String onlyDigits = phone.replaceAll("\\D+","");
            return "callto://+" + onlyDigits;
        }
    }

    public static interface RowItem {
        String getCountryCode();        //For example: UA

        String getCompany();            //For example: JavaRush Ltd.

        String getContactFirstName();   //For example: Ivan

        String getContactLastName();    //For example: Ivanov

        String getDialString();         //For example: callto://+380501234567
    }

    public static interface Customer {
        String getCompanyName();        //For example: JavaRush Ltd.

        String getCountryName();        //For example: Ukraine
    }

    public static interface Contact {
        String getName();               //For example: Ivanov, Ivan

        String getPhoneNumber();        //For example: +38(050)123-45-67 or +3(805)0123-4567 or +380(50)123-4567 or ...
    }
}