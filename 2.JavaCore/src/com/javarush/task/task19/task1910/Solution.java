package com.javarush.task.task19.task1910;

import java.io.*;
import java.util.ArrayList;

/* 
Пунктуация
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileNameFrom = reader.readLine();
            String fileNameTo = reader.readLine();

            ArrayList<String> buffer = new ArrayList<>();

            try (BufferedReader br1 = new BufferedReader(new FileReader(fileNameFrom));
                 BufferedWriter bw = new BufferedWriter(new FileWriter(fileNameTo))) {

                while (br1.ready()) {
                    buffer.add(br1.readLine());
                }

                for (String s : buffer) {
                    bw.write(s.replaceAll("\\p{Punct}", "") + System.lineSeparator());
                }
            }
        }
    }
}
