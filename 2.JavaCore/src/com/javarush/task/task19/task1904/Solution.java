package com.javarush.task.task19.task1904;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

/* 
И еще один адаптер
*/

public class Solution {

    public static void main(String[] args) {

//        String str = "Иванов Иван Иванович 31 12 1950";
//        String[] arr = str.split(" ");
//        Date dt = new GregorianCalendar(Integer.parseInt(arr[5]), Integer.parseInt(arr[4]) - 1, Integer.parseInt(arr[3])).getTime();
//        System.out.println(arr[0] + " " + arr[1] + " " + arr[2] + " " + dt);

    }

    public static class PersonScannerAdapter implements PersonScanner {

        private Scanner fileScanner;

        PersonScannerAdapter(Scanner fileScanner) {this.fileScanner = fileScanner;}

        @Override
        public Person read() throws IOException {
            String tmp = fileScanner.nextLine();
            String[] subStr = tmp.split(" ", 4);
            Date birthDate = null;

            try {
                birthDate = new SimpleDateFormat("dd MM yyyy").parse(subStr[3]);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Person person = new Person(subStr[1], subStr[2], subStr[0], birthDate);
            return person;
        }

        @Override
        public void close() throws IOException {
            this.fileScanner.close();
        }
    }
}
