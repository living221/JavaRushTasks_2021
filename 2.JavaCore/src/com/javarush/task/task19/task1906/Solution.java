package com.javarush.task.task19.task1906;

import java.io.*;
import java.util.ArrayList;

/* 
Четные символы
*/

public class Solution {
    public static void main(String[] args) throws IOException {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileNameFrom = reader.readLine();
            String fileNameTo = reader.readLine();

            FileReader readerFF = new FileReader(fileNameFrom);
            FileWriter writerTF = new FileWriter(fileNameTo);

            ArrayList<Integer> fileContent = new ArrayList<>();

            while (readerFF.ready()) {
                fileContent.add(readerFF.read());
            }

            for (int index = 0; index < fileContent.size(); index++) {
                if (index % 2 != 0) {
                    writerTF.write(fileContent.get(index));
                }
            }
            readerFF.close();
            writerTF.close();
        }
    }
}
