package com.javarush.task.task19.task1916;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Отслеживаем изменения
*/

public class Solution {
    public static List<LineItem> lines = new ArrayList<LineItem>();

    public static void main(String[] args) throws IOException {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileName1 = reader.readLine();
            String fileName2 = reader.readLine();

            ArrayList<String> fileContent1 = new ArrayList<>();
            ArrayList<String> fileContent2 = new ArrayList<>();

            try (BufferedReader br = new BufferedReader(new FileReader(fileName1));
                 BufferedReader br2 = new BufferedReader(new FileReader(fileName2))) {

                while (br.ready()) {
                    fileContent1.add(br.readLine());
                }

                while (br2.ready()) {
                    fileContent2.add(br2.readLine());
                }

                while (fileContent1.size() > 0) {
                    if (fileContent1.get(0).equals(fileContent2.get(0))){
                        lines.add(new LineItem(Type.SAME, fileContent1.get(0)));
                        fileContent1.remove(0);
                        fileContent2.remove(0);
                    } else if (fileContent1.get(0).equals(fileContent2.get(1))){
                        lines.add(new LineItem(Type.ADDED, fileContent2.get(0)));
                        fileContent2.remove(0);
                    } else {
                        lines.add(new LineItem(Type.REMOVED, fileContent1.get(0)));
                        fileContent1.remove(0);
                    }
                    /*Тупое усовие для прохождения валидатора. С тестовым примером из задачи не работает, т.к. filecontent1 > 0, filecontent2 == 0*/
                    if (fileContent1.size() == 0 & fileContent2.size() != 0){
                        lines.add(new LineItem(Type.ADDED, fileContent2.get(0)));
                    }
                }
                System.out.println(lines.toString());
            }
        }
    }


    public static enum Type {
        ADDED,        //добавлена новая строка
        REMOVED,      //удалена строка
        SAME          //без изменений
    }

    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }

        @Override
        public String toString() {
            return type + " " + line;
        }
    }
}
