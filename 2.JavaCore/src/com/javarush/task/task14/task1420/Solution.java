package com.javarush.task.task14.task1420;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.SQLOutput;

/* 
НОД
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Введите первое целое число больше ноля");
            int Integer1 = Integer.parseInt(reader.readLine());
            System.out.println("Введите второе целое число больше ноля");
            int Integer2 = Integer.parseInt(reader.readLine());

            System.out.println("Наибольший общий делитель равен " + gcd(Integer1, Integer2));
        }

    }

    public static int gcd(int a, int b) {
        if (b==0) return a;
        return gcd(b,a%b);
    }
}
