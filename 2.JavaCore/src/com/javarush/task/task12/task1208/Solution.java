package com.javarush.task.task12.task1208;

/* 
Свобода печати
*/

public class Solution {
    public static void main(String[] args) {

    }

    //Напишите тут ваши методы
    public static void print() {

    }

    public static void print(int a) {

    }

    public static void print(int a, int b, int c) {

    }

    public static void print(int a, int b, String s) {

    }

    public static void print(String s, int a, int b) {

    }


}
