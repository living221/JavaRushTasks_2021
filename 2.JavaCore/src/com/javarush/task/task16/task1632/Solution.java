package com.javarush.task.task16.task1632;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Клубок
*/

public class Solution {
    public static List<Thread> threads = new ArrayList<>(5);

    static {
        threads.add(new InfLoop());
        threads.add(new IntException());
        threads.add(new Hurray());
        threads.add(new Warning());
        threads.add(new Sum());
    }

    public static class InfLoop extends Thread {
        @Override
        public void run() {
            while (true){

            }
        }
    }

    public static class IntException extends Thread {
        @Override
        public void run() {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                System.out.println("InterruptedException");
            }
        }
    }

    public static class Hurray extends Thread {
        @Override
        public void run() {

            while (true){
                System.out.println("Ура");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class Warning extends Thread implements Message {
        private boolean isCancel = false;

        public void run(){

            while (!isCancel){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
                System.out.println("Thread 4 running");
            }
        }
        @Override
        public void showWarning() {
            isCancel = true;
        }
    }

    public static class Sum extends Thread {
        @Override
        public void run() {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            try {
                String str;
                int sum = 0;
                while (!(str = reader.readLine()).contains("N")){
                    int num = Integer.parseInt(str);
                    sum += num;
                }
                System.out.println(sum);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {


    }
}