package com.javarush.task.task17.task1721;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* 
Транзакционность
*/

public class Solution {
    public static List<String> allLines = new ArrayList<String>();
    public static List<String> forRemoveLines = new ArrayList<String>();

    public static void main(String[] args) {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileNameForAllLines = reader.readLine();
            String fileNameForRemoveLines = reader.readLine();

            File fileAllLines = new File(fileNameForAllLines);
            File fileRemoveLines = new File(fileNameForRemoveLines);

            if (fileAllLines.exists() && !fileAllLines.isDirectory()) {
                InputStream inStream = new FileInputStream(fileNameForAllLines);
                BufferedReader fileBuffer = new BufferedReader(new InputStreamReader(inStream));

                while (fileBuffer.ready()) {
                    allLines.add(fileBuffer.readLine());
                }
                System.out.println(allLines);
            }

            if (fileRemoveLines.exists() && !fileRemoveLines.isDirectory()) {
                InputStream inStream = new FileInputStream(fileRemoveLines);
                BufferedReader fileBuffer = new BufferedReader(new InputStreamReader(inStream));

                while (fileBuffer.ready()) {
                    forRemoveLines.add(fileBuffer.readLine());
                }
                System.out.println(forRemoveLines);
            }

            System.out.println(fileNameForAllLines + " " + fileNameForRemoveLines);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Solution solution = new Solution();
        try {
            solution.joinData();
        } catch (CorruptedDataException e) {
            e.printStackTrace();
            System.out.println("Corrupted data in files");
        }

    }

    public void joinData() throws CorruptedDataException {
        if (allLines.containsAll(forRemoveLines)){
            allLines.removeAll(forRemoveLines);
        } else {
            allLines.clear();
            throw new CorruptedDataException();
        }
    }
}
