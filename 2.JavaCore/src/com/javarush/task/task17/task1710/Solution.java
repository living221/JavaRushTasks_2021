package com.javarush.task.task17.task1710;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws ParseException {
        //напишите тут ваш код

        SimpleDateFormat dateFormatCreate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

        switch (args[0]) {
            case "-c":
                create(args, dateFormatCreate);
                break;
            case "-r":
                read(args);
                break;
            case "-u":
                update(args, dateFormatCreate);
                break;
            case "-d":
                delete(args);
                break;
        }
    }

    private static void create(String[] args, SimpleDateFormat dateFormatCreate) throws ParseException {
        if (args[2].equals("м")) {
            allPeople.add(Person.createMale(args[1], dateFormatCreate.parse(args[3])));
            System.out.println(allPeople.size() - 1);
        }
        if (args[2].equals("ж")) {
            allPeople.add(Person.createFemale(args[1], dateFormatCreate.parse(args[3])));
            System.out.println(allPeople.size() - 1);
        }
    }

    private static void delete(String[] args) {
        int id;
        id = Integer.parseInt(args[1]);
        allPeople.get(id).setName(null);
        allPeople.get(id).setSex(null);
        allPeople.get(id).setBirthDate(null);
    }

    private static void update(String[] args, SimpleDateFormat dateFormatCreate) throws ParseException {
        int id;
        id = Integer.parseInt(args[1]);
        allPeople.get(id).setName(args[2]);
        if (args[3].equals("м")) {
            allPeople.get(id).setSex(Sex.MALE);
        }
        if (args[3].equals("ж")) {
            allPeople.get(id).setSex(Sex.FEMALE);
        }
        allPeople.get(id).setBirthDate(dateFormatCreate.parse(args[4]));
    }

    private static void read(String[] args) {
        int id;
        id = Integer.parseInt(args[1]);
        String name = allPeople.get(id).getName();
        String sex = " ";
        String date = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH).format(allPeople.get(id).getBirthDate());

        if (allPeople.get(id).getSex() == Sex.MALE) {
            sex = "м";
        }
        if (allPeople.get(id).getSex() == Sex.FEMALE) {
            sex = "ж";
        }
        System.out.println(name + " " + sex + " " + date);
    }
}
