package com.javarush.task.task17.task1711;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD 2
*/

public class Solution {
    public static volatile List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws ParseException {
        //start here - начни тут
        SimpleDateFormat dateFormatCreate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

        switch (args[0]) {
            case "-c":
                synchronized (allPeople){
                    create(args, dateFormatCreate);
                    break;
                }
            case "-i":
                synchronized (allPeople) {
                    read(args);
                    break;
                }
            case "-u":
                synchronized (allPeople){
                    update(args, dateFormatCreate);
                    break;
                }
            case "-d":
                synchronized (allPeople) {
                    delete(args);
                    break;
                }
        }
    }

    public static void create(String[] args, SimpleDateFormat dateFormatCreate) throws ParseException {
        for (int i = 1; i < args.length; i += 3) {
            if (args[i + 1].equals("м")) {
                allPeople.add(Person.createMale(args[i], dateFormatCreate.parse(args[i + 2])));
            } else {
                allPeople.add(Person.createFemale(args[i], dateFormatCreate.parse(args[i + 2])));
            }
            System.out.println(allPeople.size() - 1);
        }
    }

    private static void delete(String[] args) {
        for (int i = 1; i < args.length; i++) {
            int id = Integer.parseInt(args[i]);
            allPeople.get(id).setName(null);
            allPeople.get(id).setSex(null);
            allPeople.get(id).setBirthDate(null);
        }
    }

    public static void update(String[] args, SimpleDateFormat dateFormatCreate) throws ParseException {
        for (int i = 1; i < args.length; i += 4) {
            int id = Integer.parseInt(args[i]);
            allPeople.get(id).setName(args[i + 1]);
            if (args[i + 2].equals("м")) {
                allPeople.get(id).setSex(Sex.MALE);
            }
            if (args[i + 2].equals("ж")) {
                allPeople.get(id).setSex(Sex.FEMALE);
            }
            allPeople.get(id).setBirthDate(dateFormatCreate.parse(args[i + 3]));
        }
    }

    private static void read(String[] args) {
        for (int i = 1; i < args.length; i++) {
            int id = Integer.parseInt(args[i]);
            String name = allPeople.get(id).getName();
            String sex = " ";
            String date = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH).format(allPeople.get(id).getBirthDate());

            if (allPeople.get(id).getSex() == Sex.MALE) {
                sex = "м";
            }
            if (allPeople.get(id).getSex() == Sex.FEMALE) {
                sex = "ж";
            }
            System.out.println(name + " " + sex + " " + date);
        }
    }
}
