package com.javarush.task.task35.task3507;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

/* 
ClassLoader - что это такое?
*/

public class Solution {
    public static void main(String[] args) {
        String debugPath = "C:/IDEA_projects/JavaRushTasks2/JavaRushTasks/out/production/4.JavaCollections/com/javarush/task/task35/task3507/data";
//        Set<? extends Animal> allAnimals = getAllAnimals(Solution.class.getProtectionDomain().getCodeSource().getLocation().getPath() + Solution.class.getPackage().getName().replaceAll("[.]", "/") + "/data");
        Set<? extends Animal> allAnimals = getAllAnimals(debugPath);
        System.out.println(allAnimals);
    }

    public static Set<? extends Animal> getAllAnimals(String pathToAnimals) {

        Set<Animal> result = new HashSet<>();

        ClassLoader loader = new AnimalClassLoader();
        File[] files = new File(pathToAnimals).listFiles();
        for (File file : files) {
            String fileName = file.toString();
            int index = fileName.lastIndexOf('.');
            String extension = null;
            if (index > 0) {
                extension = fileName.substring(index + 1);
            }
            if (file.isFile() & ("class").equals(extension)) {
                try {
                    Class clazz = ((AnimalClassLoader) loader).findClass(file.getAbsolutePath());


                    Constructor constructor = clazz.getConstructor();
                    boolean assignableFrom = Animal.class.isAssignableFrom(clazz);
                    if (constructor.getModifiers() == Modifier.PUBLIC && assignableFrom && (constructor.getParameterCount() == 0)) {
                        constructor.setAccessible(true);
                        result.add((Animal) constructor.newInstance());
                        System.out.println(result);
                    }

                } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public static class AnimalClassLoader extends ClassLoader {

        @Override
        protected Class<?> findClass(String name) throws ClassNotFoundException {
            try {
                byte[] buffer = new byte[0];
                Path file = Paths.get(name);
                buffer = Files.readAllBytes(file);
                return defineClass(null, buffer, 0, buffer.length);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return super.findClass(name);
        }

//        public Class<?> findClass(String path) throws ClassNotFoundException {
//            try {
//                byte[] buffer = new byte[0];
//                Path file = Paths.get(path);
//                buffer = Files.readAllBytes(file);
//                return defineClass(null, buffer, 0, buffer.length);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return super.findClass(path);
//        }
    }
}
