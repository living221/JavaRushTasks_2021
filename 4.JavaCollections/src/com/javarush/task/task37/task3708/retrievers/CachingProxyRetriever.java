package com.javarush.task.task37.task3708.retrievers;

import com.javarush.task.task37.task3708.cache.LRUCache;
import com.javarush.task.task37.task3708.storage.Storage;

public class CachingProxyRetriever implements Retriever {
    OriginalRetriever originalRetriever;
    LRUCache cache;

    public CachingProxyRetriever(Storage storage) {
        originalRetriever = new OriginalRetriever(storage);
        cache = new LRUCache(16);
    }

    @Override
    public Object retrieve(long id) {
        Object result = cache.find(id);
        if (result != null) {
            return result;
        } else {
            Object retrieve = originalRetriever.retrieve(id);
            cache.set(id, retrieve);
            return cache.find(id);
        }
    }
}
