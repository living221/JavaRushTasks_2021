package com.javarush.task.task37.task3714;

import org.junit.Assert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/* 
Древний Рим
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Input a roman number to be converted to decimal: ");
        String romanString = bufferedReader.readLine();
        System.out.println("Conversion result equals " + romanToInteger(romanString));

//        Assert.assertEquals(romanToInteger("IV"), 4);
//        Assert.assertEquals(romanToInteger("VI"), 6);
//        Assert.assertEquals(romanToInteger("VIII"), 8);
//        Assert.assertEquals(romanToInteger("IX"), 9);
//        Assert.assertEquals(romanToInteger("XV"), 15);
//        Assert.assertEquals(romanToInteger("XIX"), 19);
//        Assert.assertEquals(romanToInteger("XL"), 40);
//        Assert.assertEquals(romanToInteger("XLII"), 42);
//        Assert.assertEquals(romanToInteger("LX"), 60);
//        Assert.assertEquals(romanToInteger("LXXX"), 80);
//        Assert.assertEquals(romanToInteger("LXXXIII"), 83);
//        Assert.assertEquals(romanToInteger("XCIV"), 94);
//        Assert.assertEquals(romanToInteger("XC"), 90);
//        Assert.assertEquals(romanToInteger("CL"), 150);
//        Assert.assertEquals(romanToInteger("CCLXXXIII"), 283);
//        Assert.assertEquals(romanToInteger("DCCC"), 800);
//        Assert.assertEquals(romanToInteger("MCMLXXXVIII"), 1988);
//        Assert.assertEquals(romanToInteger("MMDCLXXXIII"), 2683);
//        Assert.assertEquals(romanToInteger("MMDDCCLLXXVVII"), 3332);
//        Assert.assertEquals(romanToInteger("MMMD"), 3500);

    }

    public static int romanToInteger(String s) {
        int result = 0;

        Map<Character, Integer> romanToInt = new HashMap<>();
        romanToInt.put('I', 1);
        romanToInt.put('V', 5);
        romanToInt.put('X', 10);
        romanToInt.put('L', 50);
        romanToInt.put('C', 100);
        romanToInt.put('D', 500);
        romanToInt.put('M', 1000);
        
        char[] chars = s.toCharArray();

        int tmpValue = 0;
        for (int i = chars.length; i > 0; i--) {
            for (Map.Entry<Character, Integer> entry : romanToInt.entrySet()) {
                if (entry.getKey().equals(chars[i - 1])) {
                    if (entry.getValue() < tmpValue) {
                        result -= entry.getValue();
                    } else {
                        result += entry.getValue();
                    }
                    tmpValue = entry.getValue();
                }
            }
        }

        return result;
    }
}
