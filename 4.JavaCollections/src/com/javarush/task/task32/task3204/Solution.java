package com.javarush.task.task32.task3204;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
//import java.nio.charset.StandardCharsets;

/* 
Генератор паролей
*/

public class Solution {
    public static void main(String[] args) {
        ByteArrayOutputStream password = getPassword();
        System.out.println(password.toString());
    }

    public static ByteArrayOutputStream getPassword() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        String password = "";
        int length = 8;

        for (int i = 0; i < length - 2; i++) {
            password = password + randomCharacter("abcdefghijklmnopqrstuvwxyz");
        }

        String randomDigit = randomCharacter("0123456789");
        String randomUpperCaseCharacter = randomCharacter("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        password = insertAtRandom(password, randomDigit);
        password = insertAtRandom(password, randomUpperCaseCharacter);

        try {
            baos.write(password.getBytes());
        } catch (IOException e) {
            System.out.println("IO error occurred during password creation");
        }
        return baos;
    }

    private static String randomCharacter(String characters) {
        int n = characters.length();
        int r = (int) (n * Math.random());
        return characters.substring(r, r + 1);
    }

    private static String insertAtRandom(String str, String toInsert) {
        int n = str.length();
        int r = (int) ((n + 1) * Math.random());
        return str.substring(0, r) + toInsert + str.substring(r);
    }
}
