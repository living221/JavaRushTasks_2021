package com.javarush.task.task32.task3210;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;

/* 
Используем RandomAccessFile
*/

public class Solution {
    public static void main(String... args) {
        String filename = args[0];
        String number = args[1];
        String text = args[2];

        byte[] document = new byte[text.length()];

        try {
            RandomAccessFile raf = new RandomAccessFile(filename, "rw");
            raf.seek(Long.parseLong(number));
            raf.read(document, 0, text.length());

            if (new String(document, StandardCharsets.UTF_8).equals(text)) {
                raf.seek(raf.length());
                raf.write("true".getBytes(StandardCharsets.UTF_8));
            } else {
                raf.seek(raf.length());
                raf.write("false".getBytes(StandardCharsets.UTF_8));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
