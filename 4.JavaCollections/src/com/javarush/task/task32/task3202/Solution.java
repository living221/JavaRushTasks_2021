package com.javarush.task.task32.task3202;

import java.io.*;
import java.util.Objects;

/* 
Читаем из потока
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        StringWriter writer = getAllDataFromInputStream(new FileInputStream("testFile.log"));
        System.out.println(writer.toString());
    }

    public static StringWriter getAllDataFromInputStream(InputStream is) throws IOException {
        if (is == null) {
            return new StringWriter();
        }
        final int TRANSFER_BUFFER_SIZE = 8192;
        StringWriter stringWriter = new StringWriter();
        InputStreamReader inputStreamReader = new InputStreamReader(is);

        char[] buffer = new char[TRANSFER_BUFFER_SIZE];
        int nRead;
        while ((nRead = inputStreamReader.read(buffer, 0, TRANSFER_BUFFER_SIZE)) >= 0) {
            stringWriter.write(buffer, 0, nRead);
        }
        return stringWriter;
    }
}
