package com.javarush.task.task32.task3205;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;

public class CustomInvocationHandler implements InvocationHandler {

    SomeInterfaceWithMethods target;

    public CustomInvocationHandler(SomeInterfaceWithMethods someInterfaceWithMethods) {
        this.target = someInterfaceWithMethods;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println(method.getName() + " in");
        Object invoke = method.invoke(target, args);
        System.out.println(method.getName() + " out");
        return invoke;
    }
}
