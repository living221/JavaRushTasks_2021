package com.javarush.task.task32.task3201;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;

/* 
Запись в существующий файл
*/

public class Solution {
    public static void main(String... args) {
        String filename = args[0];
        String number = args[1];
        String text = args[2];

        try {
            RandomAccessFile raf = new RandomAccessFile(filename, "rw");
            if (Long.parseLong(number) > raf.length()) {
                raf.seek(raf.length());
            } else {
                raf.seek(Long.parseLong(number));
            }
            raf.write(text.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
