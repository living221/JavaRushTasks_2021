package com.javarush.task.task20.task2028;

import java.io.Serializable;
import java.util.*;

/* 
Построй дерево(1)
*/

public class CustomTree extends AbstractList<String> implements Cloneable, Serializable {

    Entry<String> root;
    int size;
    List<Entry<String>> list = new LinkedList<>();

    public CustomTree() {
        this.root = new Entry<>("ROOT");
        size = 0;
        list.add(root);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean add(String s) {
        Entry<String> entry = new Entry<>(s);
        for (Entry<String> each : list) {
            if (each.isAvailableToAddChildren()) {
                if (each.availableToAddLeftChildren) {
                    each.leftChild = entry;
                    each.availableToAddLeftChildren = false;
                }
                else {
                    each.rightChild = entry;
                    each.availableToAddRightChildren = false;
                }
                entry.parent = each;
                list.add(entry);
                size++;
                return true;
            }
        }
        for (Entry<String> each : list) {
            if (each.hasParent() && !each.hasChildren()) {
                each.availableToAddLeftChildren = true;
                each.availableToAddRightChildren = true;
            }
        }
        this.add(s);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (!(o instanceof String)) {
            throw new UnsupportedOperationException();
        }
        for (Entry<String> each : list) {
            if (each.elementName.equals(o)) {
                return removeRecursively(each);
            }
        }
        return false;
    }

    private boolean removeRecursively (Entry<String> child) {
        if (!child.availableToAddLeftChildren) removeRecursively(child.leftChild);
        if (!child.availableToAddRightChildren) removeRecursively(child.rightChild);
        child.leftChild = null;
        child.rightChild = null;
        if (child.getParent().leftChild == null ||
                child.getParent().leftChild.equals(child)) child.getParent().leftChild = null;
        if (child.getParent().rightChild == null ||
                child.getParent().rightChild.equals(child)) child.getParent().rightChild = null;
        size--;
        list.remove(child);
        return true;
    }

    public String getParent(String s) {
        String name = null;
        try {
            for (Entry<String> each : list) {
                if (each.elementName.equals(s)) name = each.parent.elementName;
            }
        } catch (NullPointerException ignored) {}
        return name;
    }


    @Override
    public String get(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String set(int index, String element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int index, String element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String remove(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, Collection<? extends String> c) {
        throw new UnsupportedOperationException();
    }

    static class Entry<T> implements Serializable {
        String elementName;
        boolean availableToAddLeftChildren, availableToAddRightChildren;
        Entry<T> parent, leftChild, rightChild;

        public Entry(String elementName) {
            this.elementName = elementName;
            availableToAddLeftChildren = true;
            availableToAddRightChildren = true;
        }

        public boolean isAvailableToAddChildren() {
            return availableToAddLeftChildren || availableToAddRightChildren;
        }

        public boolean hasParent() {
            return !(this.parent == null);
        }

        public Entry<T> getParent() {
            return parent;
        }

        public boolean hasChildren() {
            return (this.leftChild != null) || (this.rightChild != null);
        }
    }
}
