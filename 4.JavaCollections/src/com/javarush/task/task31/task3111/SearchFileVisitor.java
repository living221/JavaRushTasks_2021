package com.javarush.task.task31.task3111;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class SearchFileVisitor extends SimpleFileVisitor<Path> {

    private String partOfName = "";
    private String partOfContent = "";
    private int minSize = 0;
    private int maxSize = Integer.MAX_VALUE;
    private List<Path> foundFiles = new ArrayList<>();

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        byte[] contentAr = Files.readAllBytes(file); // размер файла: content.length
        String content = new String(Files.readAllBytes(file));

        if (file.getFileName().toString().contains(partOfName)
                && content.contains(partOfContent)
                && contentAr.length > minSize
                && contentAr.length < maxSize)
            foundFiles.add(file);

        return super.visitFile(file, attrs);
    }

    public void setPartOfName(String partOfName) {
        this.partOfName = partOfName;
    }

    public void setPartOfContent(String partOfContent) {
        this.partOfContent = partOfContent;
    }

    public void setMinSize(int minFileSize) {
        this.minSize = minFileSize;
    }

    public void setMaxSize(int maxFileSize) {
        this.maxSize = maxFileSize;
    }

    public List<Path> getFoundFiles() {
        return foundFiles;
    }
}
