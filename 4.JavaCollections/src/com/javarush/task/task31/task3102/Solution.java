package com.javarush.task.task31.task3102;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

/* 
Находим все файлы
*/

public class Solution {
    public static List<String> getFileTree(String root) throws IOException {

        List<String> result = new ArrayList<>();

        List<Path> allFiles;
        allFiles = Files.walk(Paths.get(root))
                .filter(Files::isRegularFile)
                .collect(Collectors.toList());

        for (Path path :
                allFiles) {
            result.add(path.toString());
        }
        return result;
    }


    public static void main(String[] args) throws IOException {
        String root = "C:\\Users\\dmitr\\Desktop\\test\\3101";
        List<String> fileTree = getFileTree(root);
        for (String fileName :
                fileTree) {
            System.out.println(fileName);
        }
    }
}
