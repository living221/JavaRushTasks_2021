package com.javarush.task.task31.task3101;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.stream.Collectors;

/* 
Проход по дереву файлов
*/

public class Solution {
    public static void main(String[] args) throws Exception {

        File file = new File(args[1]);

        File newFile = new File(file.getParent() + "/allFilesContent.txt");

        FileUtils.renameFile(file, newFile);

        List<Path> allFiles;
        allFiles = Files.walk(Paths.get(args[0]))
                .filter(Files::isRegularFile)
                .collect(Collectors.toList());

        FileOutputStream bos = new FileOutputStream(newFile);
        bos.close();

        for (Path path : allFiles) {
            if (new File(path.toString()).length() <= 50) {
                try (FileInputStream in = new FileInputStream(path.toString());
                     FileOutputStream out = new FileOutputStream(newFile, true)) {
                    int n;
                    while ((n = in.read()) != -1) {
                        out.write(n);
                    }
                    out.write('\n');
                }
            }
        }
    }
}
