package com.javarush.task.task31.task3113;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/* 
Что внутри папки?
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
*/

public class Solution {


    public static int directoryCounter = 0;
    public static int filesCounter = 0;
    public static int bytesCounter = 0;


    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String directoryName = reader.readLine();
            Path directory = Paths.get(directoryName);
            if (!Files.isDirectory(directory)) {
                System.out.println(directory + " - не папка");
                return;
            }

            Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    filesCounter++;
                    bytesCounter += Files.size(file);
                    return super.visitFile(file, attrs);
                }

                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    directoryCounter++;
                    return super.preVisitDirectory(dir, attrs);
                }
            });

            System.out.println("Параметры папки " + directory.toString() + ":");
            System.out.println("Всего папок - " + (directoryCounter - 1));
            System.out.println("Всего файлов - " + filesCounter);
            System.out.println("Общий размер - " + bytesCounter);
        }
    }
}
