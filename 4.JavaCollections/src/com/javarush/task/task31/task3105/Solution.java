package com.javarush.task.task31.task3105;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/* 
Добавление файла в архив
*/

public class Solution {
    public static void main(String[] args) throws IOException {

        Map<ZipEntry, ByteArrayOutputStream> map = new HashMap<>();

        try (ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(args[1]))) {

            ZipEntry zipEntry;
            ByteArrayOutputStream baos;
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                baos = new ByteArrayOutputStream();
                byte[] buff = new byte[8 * 1024];
                int len;
                while ((len = zipInputStream.read(buff)) != -1) {
                    baos.write(buff, 0, len);
                }
                map.put(zipEntry, baos);
                zipInputStream.closeEntry();
            }
        }

        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(args[1]))) {
            boolean fileExists = false;
            //System.out.println();

            for (Map.Entry<ZipEntry, ByteArrayOutputStream> entry : map.entrySet()) {
                if (!String.valueOf(Paths.get(entry.getKey().toString()).getFileName()).equals(Paths.get(args[0]).getFileName().toString())) {
                    zos.putNextEntry(new ZipEntry(entry.getKey().toString()));
                    //System.out.println("first if " + entry.getKey());
                    zos.write(entry.getValue().toByteArray(), 0, entry.getValue().toByteArray().length);
                    zos.closeEntry();
                } else {
                    zos.putNextEntry(new ZipEntry(entry.getKey().toString()));
                    //System.out.println("Else: " + entry.getKey());
                    Files.copy(Paths.get(args[0]), zos);
                    zos.closeEntry();
                    fileExists = true;
                }

            }
            if (!fileExists) {
                zos.putNextEntry(new ZipEntry("new/" + Paths.get(args[0]).getFileName()));
                Files.copy(Paths.get(args[0]), zos);
                zos.closeEntry();
            }

        }

//            File fileToAdd = new File(args[0]);
//            ZipEntry zipEntry = new ZipEntry("new/" + fileToAdd.getName());
//            zipOutputStream.putNextEntry(zipEntry);
//            //adding new file to existing zrchive
//            File file = new File(args[0]);
//            Files.copy(file.toPath(), zipOutputStream);
//            String zipArchiveName = zipEntry.toString();
//            zipOutputStream.closeEntry();
//
//            for (Map.Entry<String, ByteArrayOutputStream> entry : map.entrySet()) {
//                ZipEntry zipEntryTmp = new ZipEntry(entry.getKey());
//                String zipEntryString = zipEntryTmp.getName();
//                if (zipEntryString.equals(zipArchiveName) || zipEntryString.endsWith(fileToAdd.getName())) {
//                    continue;
//                }
//                zipOutputStream.putNextEntry(new ZipEntry(entry.getKey()));
//                zipOutputStream.write(entry.getValue().toByteArray());
//                zipOutputStream.closeEntry();
//            }
    }
}
