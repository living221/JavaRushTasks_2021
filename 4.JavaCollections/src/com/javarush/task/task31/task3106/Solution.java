package com.javarush.task.task31.task3106;

import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/* 
Разархивируем файл
*/

public class Solution {
    public static void main(String[] args) throws IOException {

        List<String> zipParts = new ArrayList<>();
        for (int i = 1; i < args.length; i++) {
            zipParts.add(args[i]);
        }
        Collections.sort(zipParts);

//        for (String name :
//                zipParts) {
//            System.out.println(name);
//        }

        List<FileInputStream> zipPartsStreams = new ArrayList<>();
        for (String zipPartName : zipParts) {
            try {
                zipPartsStreams.add(new FileInputStream(zipPartName));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        try (ZipInputStream zipInputStream = new ZipInputStream(new SequenceInputStream(Collections.enumeration(zipPartsStreams)));
             FileOutputStream fileOutputStream = new FileOutputStream(args[0], true)) {
            while (zipInputStream.getNextEntry() != null) {
                    byte[] buff = new byte[8 * 1024];
                int len;
                while ((len = zipInputStream.read(buff)) != -1) {
                    fileOutputStream.write(buff, 0, len);
                }
            }
        }
    }
}
