package com.javarush.task.task40.task4009;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;

/* 
Buon Compleanno!
*/

public class Solution {
    public static void main(String[] args) {
        System.out.println(getWeekdayOfBirthday("30.05.1984", "2015"));
        System.out.println(getWeekdayOfBirthday("01.12.2015", "2016"));
    }

    public static String getWeekdayOfBirthday(String birthday, String year) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.y");
        LocalDate date = LocalDate.parse(birthday, formatter);
        Year year1 = Year.parse(year);
        LocalDate date2 = date.withYear(year1.getValue());
        DayOfWeek day = date2.getDayOfWeek();
        return day.getDisplayName(TextStyle.FULL, Locale.ITALIAN);
    }
}
