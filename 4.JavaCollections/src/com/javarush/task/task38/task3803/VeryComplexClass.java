package com.javarush.task.task38.task3803;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* 
Runtime исключения (unchecked exception)
*/

public class VeryComplexClass {
    public void methodThrowsClassCastException() {
        Object obj2 = new Object();
        VeryComplexClass veryComplexClass = (VeryComplexClass) obj2;
    }

    public void methodThrowsNullPointerException() {
        Object obj = new Object();
        Object obj1 = null;

        if (obj1.equals(obj)) {
            System.out.println("success!");
        }
    }

    public static void main(String[] args) {
//        VeryComplexClass veryComplexClass = new VeryComplexClass();
//        veryComplexClass.methodThrowsNullPointerException();
    }
}
