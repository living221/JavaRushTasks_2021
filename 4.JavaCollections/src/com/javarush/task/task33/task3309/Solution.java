package com.javarush.task.task33.task3309;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/* 
Комментарий внутри xml
*/

public class Solution {
    public static String toXmlWithComment(Object obj, String tagName, String comment) throws ParserConfigurationException, JAXBException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setCoalescing(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();

        JAXBContext context = JAXBContext.newInstance(obj.getClass());
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(obj, document);

        NodeList elementsByTagName = document.getElementsByTagName(tagName);
        for (int i = 0; i < elementsByTagName.getLength(); i++) {
            Node node = elementsByTagName.item(i);
            if (node.getNodeName().equals(tagName)) {
                node.getParentNode().insertBefore(document.createComment(comment), node);
                node.getParentNode().insertBefore(document.createTextNode("\n"), node);
            }
            if ((node.getFirstChild().getNodeType() == Node.TEXT_NODE) && (node.getTextContent().matches(".*[<>&'\"].*"))) {
                String nodeContent = node.getTextContent();
                Node firstChild = node.getFirstChild();
                CDATASection cdataSection = document.createCDATASection(nodeContent);
                node.replaceChild(cdataSection, firstChild);
            }
        }

        StringWriter writer = new StringWriter();
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.STANDALONE, "no");
        transformer.transform(new DOMSource(document), new StreamResult(writer));
        return writer.toString();
    }

    public static void main(String[] args) throws TransformerException, JAXBException, ParserConfigurationException {
        System.out.println(Solution.toXmlWithComment(new First(), "second", "it's a comment"));
    }
}
