package com.javarush.task.task33.task3310.tests;

import com.javarush.task.task33.task3310.Helper;
import com.javarush.task.task33.task3310.Shortener;
import com.javarush.task.task33.task3310.Solution;
import com.javarush.task.task33.task3310.strategy.HashBiMapStorageStrategy;
import com.javarush.task.task33.task3310.strategy.HashMapStorageStrategy;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class SpeedTest {
    public long getTimeToGetIds(Shortener shortener, Set<String> strings, Set<Long> ids) {

        Date startGetIds = new Date();
        for (String s : strings) {
            ids.add(shortener.getId(s));
        }
        Date finishGetIds = new Date();
        return finishGetIds.getTime() - startGetIds.getTime();
    }

    public long getTimeToGetStrings(Shortener shortener, Set<Long> ids, Set<String> strings) {

        Date startGetStrings = new Date();
        for (Long id : ids) {
            strings.add(shortener.getString(id));
        }
        Date finishGetStrings = new Date();
        return finishGetStrings.getTime() - startGetStrings.getTime();
    }

    @Test
    public void testHashMapStorage() {

        HashMapStorageStrategy hashMapStorageStrategy = new HashMapStorageStrategy();
        Shortener shortener1 = new Shortener(hashMapStorageStrategy);

        HashBiMapStorageStrategy hashBiMapStorageStrategy = new HashBiMapStorageStrategy();
        Shortener shortener2 = new Shortener(hashBiMapStorageStrategy);

        Set<String> origStrings = new HashSet<>();
        for (int i = 0; i < 10000; i++) {
            origStrings.add(Helper.generateRandomString());
        }

        Set<Long> ids1 = new HashSet<>();
        Set<String> strings1 = new HashSet<>();
        long shortener1_TimeToGetIds = getTimeToGetIds(shortener1, origStrings, ids1);
        long shortener1_TimeToGetStrings = getTimeToGetStrings(shortener1, ids1, strings1);

        Set<Long> ids2 = new HashSet<>();
        Set<String> strings2 = new HashSet<>();
        long shortener2_TimeToGetIds = getTimeToGetIds(shortener2, origStrings, ids2);
        long shortener2_TimeToGetStrings = getTimeToGetStrings(shortener2, ids2, strings2);

        Assert.assertTrue(shortener1_TimeToGetIds > shortener2_TimeToGetIds);
        Assert.assertEquals(shortener1_TimeToGetStrings, shortener2_TimeToGetStrings, 30);
    }
}
