package com.javarush.task.task33.task3310.tests;

import com.javarush.task.task33.task3310.Shortener;
import com.javarush.task.task33.task3310.strategy.*;
import org.junit.Assert;
import org.junit.Test;

public class FunctionalTest {
    public void testStorage(Shortener shortener) {
        String testString_1 = "TestString_12345";
        String testString_2 = "Test_StringNumber_Two";
        String testString_3 = "TestString_12345";

        Long String_1_id = shortener.getId(testString_1);
        Long String_2_id = shortener.getId(testString_2);
        Long String_3_id = shortener.getId(testString_3);

        Assert.assertNotEquals(String_2_id, String_1_id);
        Assert.assertNotEquals(String_2_id, String_3_id);

        Assert.assertEquals(String_1_id, String_3_id);

        String StringFromId_1 = shortener.getString(String_1_id);
        String StringFromId_2 = shortener.getString(String_2_id);
        String StringFromId_3 = shortener.getString(String_3_id);

        Assert.assertEquals(testString_1, StringFromId_1);
        Assert.assertEquals(testString_2, StringFromId_2);
        Assert.assertEquals(testString_3, StringFromId_3);
    }

    @Test
    public void testHashMapStorageStrategy() {
        HashMapStorageStrategy hashMapStorageStrategy = new HashMapStorageStrategy();
        Shortener shortener = new Shortener(hashMapStorageStrategy);
        testStorage(shortener);
    }
    @Test
    public void testOurHashMapStorageStrategy() {
        OurHashMapStorageStrategy hashMapStorageStrategy = new OurHashMapStorageStrategy();
        Shortener shortener = new Shortener(hashMapStorageStrategy);
        testStorage(shortener);
    }
    @Test
    public void testFileStorageStrategy() {
        FileStorageStrategy fileStorageStrategy = new FileStorageStrategy();
        Shortener shortener = new Shortener(fileStorageStrategy);
        testStorage(shortener);
    }
    @Test
    public void testHashBiMapStorageStrategy() {
        HashBiMapStorageStrategy hashBiMapStorageStrategy = new HashBiMapStorageStrategy();
        Shortener shortener = new Shortener(hashBiMapStorageStrategy);
        testStorage(shortener);
    }
    @Test
    public void testDualHashBidiMapStorageStrategy() {
        DualHashBidiMapStorageStrategy dualHashBidiMapStorageStrategy = new DualHashBidiMapStorageStrategy();
        Shortener shortener = new Shortener(dualHashBidiMapStorageStrategy);
        testStorage(shortener);
    }
    @Test
    public void testOurHashBiMapStorageStrategy() {
        OurHashBiMapStorageStrategy ourHashBiMapStorageStrategy = new OurHashBiMapStorageStrategy();
        Shortener shortener = new Shortener(ourHashBiMapStorageStrategy);
        testStorage(shortener);
    }
}
