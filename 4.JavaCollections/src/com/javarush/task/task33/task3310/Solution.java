package com.javarush.task.task33.task3310;

import com.javarush.task.task33.task3310.strategy.*;
import com.javarush.task.task33.task3310.tests.FunctionalTest;
import com.javarush.task.task33.task3310.tests.SpeedTest;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Solution {
    public static void main(String[] args) {
//        HashMapStorageStrategy hashMapStorageStrategy = new HashMapStorageStrategy();
//        testStrategy(hashMapStorageStrategy, 10000);
//
//        OurHashMapStorageStrategy ourHashMapStorageStrategy = new OurHashMapStorageStrategy();
//        testStrategy(ourHashMapStorageStrategy, 10000);
//
//        FileStorageStrategy fileStorageStrategy = new FileStorageStrategy();
//        testStrategy(fileStorageStrategy, 500);
//
//        OurHashBiMapStorageStrategy ourHashBiMapStorageStrategy = new OurHashBiMapStorageStrategy();
//        testStrategy(ourHashBiMapStorageStrategy, 10000);
//
//        HashBiMapStorageStrategy hashBiMapStorageStrategy = new HashBiMapStorageStrategy();
//        testStrategy(hashBiMapStorageStrategy, 10000);
//
//        DualHashBidiMapStorageStrategy dualHashBidiMapStorageStrategy = new DualHashBidiMapStorageStrategy();
//        testStrategy(dualHashBidiMapStorageStrategy, 10000);

        FunctionalTest functionalTest = new FunctionalTest();
        functionalTest.testHashMapStorageStrategy();
        functionalTest.testOurHashMapStorageStrategy();
        functionalTest.testFileStorageStrategy();
        functionalTest.testHashBiMapStorageStrategy();
        functionalTest.testDualHashBidiMapStorageStrategy();
        functionalTest.testOurHashBiMapStorageStrategy();

        SpeedTest speedTest = new SpeedTest();
        speedTest.testHashMapStorage();
    }

    public static Set<Long> getIds(Shortener shortener, Set<String> strings) {

        Set<Long> Ids = new HashSet<>();
        for (String s : strings) {
            Ids.add(shortener.getId(s));
        }
        return Ids;
    }

    public static Set<String> getStrings(Shortener shortener, Set<Long> keys) {
        Set<String> strings = new HashSet<>();
        for (Long key : keys) {
            strings.add(shortener.getString(key));
        }
        return strings;
    }

    public static void testStrategy(StorageStrategy strategy, long elementsNumber) {
        System.out.println(strategy.getClass().getSimpleName());

        Set<String> strings = new HashSet<>();
        for (int i = 0; i < elementsNumber; i++) {
            strings.add(Helper.generateRandomString());
        }
        Shortener shortener = new Shortener(strategy);

        Date startGetIds = new Date();
        Set<Long> ids = getIds(shortener, strings);
        Date finishGetIds = new Date();
        Helper.printMessage("elapsed time for getIds :" + (finishGetIds.getTime() - startGetIds.getTime()) + " ms");

        Date startGetStrings = new Date();
        Set<String> stringsFromStorage = getStrings(shortener, ids);
        Date finishGetStrings = new Date();
        Helper.printMessage("elapsed time for getStrings :" + (finishGetStrings.getTime() - startGetStrings.getTime()) + " ms");

        if (strings.equals(stringsFromStorage)) {
            Helper.printMessage("Тест пройден.");
        } else {
            Helper.printMessage("Тест не пройден.");
        }
    }
}
