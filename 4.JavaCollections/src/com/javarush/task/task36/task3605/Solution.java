package com.javarush.task.task36.task3605;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/* 
Использование TreeSet
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        byte[] fileContent = Files.readAllBytes(Paths.get(args[0]));

        TreeSet<Character> treeSet = new TreeSet<>();

        for (byte b : fileContent) {
            char c = (char) b;
            if (Character.isLetter(c)) {
                treeSet.add(Character.toLowerCase(c));
            }
        }

        treeSet.stream().limit(5).forEach(System.out::print);

        /*------------------------------------------------------------------
        Этот вариант не проходит валидацию но тоже работает*/


        /*BufferedReader in = new BufferedReader(new FileReader(args[0]));
        List<String> fileContent = new ArrayList<>();
        while (in.ready()) {
            fileContent.add(in.readLine());
        }

        List<String> words = new ArrayList<>();
        for (String s : fileContent) {
            words.add(s.replaceAll("[^A-Za-z]", ""));
        }

        TreeSet<String> treeSet = new TreeSet<>();

        List<String[]> letters = new ArrayList<>();
        for (String s : words) {
            letters.add(s.split(""));
        }

        for (String[] letter : letters) {
            for (String s : letter) {
                treeSet.add(s.toLowerCase(Locale.ROOT));
            }
        }
        treeSet.stream().limit(5).forEach(System.out::print);*/
    }
}
