package com.javarush.task.task36.task3602;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* 
Найти класс по описанию Ӏ Java Collections: 6 уровень, 6 лекция
*/

public class Solution {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
        System.out.println(getExpectedClass());
    }

    public static Class getExpectedClass() throws IllegalAccessException, InstantiationException {
//        Class returnClazz = null;
//        Class clazz = Collections.class;
//        Class[] declaredClasses = clazz.getDeclaredClasses();
//        ArrayList<Class> classesWithListInterface = new ArrayList<>();
//        ArrayList<Class> classesWithPrivateStaticMod = new ArrayList<>();
//        for (Class clazzz : declaredClasses) {
//            if (List.class.isAssignableFrom(clazzz)) {
//                classesWithListInterface.add(clazzz);
//            }
//        }
//
//        for (Class classWithList : classesWithListInterface) {
//            int modifiers = classWithList.getModifiers();
//            if ((Modifier.toString(modifiers)).equals("private static")) {
//                classesWithPrivateStaticMod.add(classWithList);
//            }
//        }
//
//        for (Class classWithPS : classesWithPrivateStaticMod) {
//            Method[] declaredMethods = classWithPS.getDeclaredMethods();
//            for (Method method : declaredMethods) {
//                if (method.toString().contains(".get(int)")) {
//                    try {
//                        Method declaredMethod = classWithPS.getDeclaredMethod("get", int.class);
//                        declaredMethod.setAccessible(true);
//                        Constructor<?> constructor = classWithPS.getDeclaredConstructor();
//                        constructor.setAccessible(true);
//                        declaredMethod.invoke(constructor.newInstance(), 0);
//                    } catch (NoSuchMethodException | InvocationTargetException e) {
//
//                    } catch (IndexOutOfBoundsException e) {
//                        return classWithPS;
//                    }
////                        int parameterCount = classWithPS.getDeclaredConstructor().getParameterCount();
////                        if (parameterCount == 0) {
////                            Constructor declaredConstructor = classWithPS.getDeclaredConstructor();
////                            declaredConstructor.setAccessible(true);
////                            declaredMethod.invoke(declaredConstructor.newInstance(1));
////                        }
////
////                    } catch (InvocationTargetException e) {
////                        if (e.getCause().equals("IndexOutOfBoundsException")) {
////                            returnClazz = classWithPS;
////                        }
////                    } catch (NoSuchMethodException | IllegalAccessException | InstantiationException e) {
////                        e.printStackTrace();
////                    }
//                }
//            }
//        }
//        return returnClazz;
//    }
//        Class<?>[] classes = Collections.class.getDeclaredClasses();
//        for (Class<?> clazz : classes){
//            if (isImplementingList(clazz) && Modifier.isStatic(clazz.getModifiers()) && Modifier.isPrivate(clazz.getModifiers())){
//                try {
//                    Method method = clazz.getDeclaredMethod("get", int.class);
//                    method.setAccessible(true);
//                    Constructor<?> constructor = clazz.getDeclaredConstructor();
//                    constructor.setAccessible(true);
//                    method.invoke(constructor.newInstance(), 2);
//                }
//                catch (NoSuchMethodException | InstantiationException | IllegalAccessException e) {}
//                catch (InvocationTargetException e){
//                    if (e.getCause().toString().contains("IndexOutOfBoundsException")) return clazz;
//                }
//            }
//
//        }
//        return null;
//    }
//
//    public static boolean isImplementingList(Class<?> clazz) {
//        ArrayList<Class<?>> classInterfaces = new ArrayList<>(Arrays.asList(clazz.getInterfaces()));
//        ArrayList<Class<?>> parentInterfaces = new ArrayList<>(Arrays.asList(clazz.getSuperclass().getInterfaces()));
//        return classInterfaces.contains(List.class) || parentInterfaces.contains(List.class);
//    }
        for (Class clazz : Collections.class.getDeclaredClasses()) {
            if (clazz.getSimpleName().equals("EmptyList")) {
                return clazz;
            }
        }
        return null;
    }
}