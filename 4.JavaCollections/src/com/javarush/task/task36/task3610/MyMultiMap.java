package com.javarush.task.task36.task3610;

import java.io.Serializable;
import java.util.*;

public class MyMultiMap<K, V> extends HashMap<K, V> implements Cloneable, Serializable {
    static final long serialVersionUID = 123456789L;
    private HashMap<K, List<V>> map;
    private int repeatCount;

    public MyMultiMap(int repeatCount) {
        this.repeatCount = repeatCount;
        map = new HashMap<>();
    }

    @Override
    public int size() {
        return values().size();
        //напишите тут ваш код
    }

    @Override
    public V put(K key, V value) {
        //напишите тут ваш код

        if (!map.containsKey(key)) {
            List<V> values = new ArrayList<>();
            values.add(value);
            map.put(key, values);
            return null;
        } else {
            List<V> vs = map.get(key);
            V lastValue = vs.get(vs.size() - 1);
            if (vs.size() < repeatCount) {
                vs.add(value);
            } else if (vs.size() == repeatCount) {
                vs.remove(0);
                vs.add(value);
            }
            map.put(key, vs);
            return lastValue;
        }
    }

    @Override
    public V remove(Object key) {
        //напишите тут ваш код
        if (!map.containsKey(key)) {
            return null;
        } else {
            List<V> values = map.get(key);
            V firstValue = values.get(0);
            values.remove(0);
            if (values.isEmpty()) {
                map.remove(key);
            } else {
                map.put((K) key, values);
            }
            return firstValue;
        }
    }

    @Override
    public Set<K> keySet() {
        //напишите тут ваш код
        Set<K> keys = new HashSet<>();
        keys = map.keySet();
        return keys;
    }

    @Override
    public Collection<V> values() {
        //напишите тут ваш код
        ArrayList<V> values = new ArrayList<>();
        for (List<V> vs : map.values()) {
            values.addAll(vs);
        }
        return values;
    }

    @Override
    public boolean containsKey(Object key) {
        //напишите тут ваш код
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        //напишите тут ваш код

        return values().contains(value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        for (Map.Entry<K, List<V>> entry : map.entrySet()) {
            sb.append(entry.getKey());
            sb.append("=");
            for (V v : entry.getValue()) {
                sb.append(v);
                sb.append(", ");
            }
        }
        String substring = sb.substring(0, sb.length() - 2);
        return substring + "}";
    }
}