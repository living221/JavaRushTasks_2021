package com.javarush.task.task39.task3910;

/* 
isPowerOfThree
*/

import java.util.Arrays;

public class Solution {
    public static void main(String[] args) {
    }

    // проверка является ли число степенью 3
    public static boolean isPowerOfThree(int n) {

        double p = Math.log10(n) / Math.log10(3);
        // checking to see if power is an integer or not
        if (p - (int) p == 0) {
            return true;
        } else {
            return false;
        }
    }
}
