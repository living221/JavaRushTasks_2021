package com.javarush.task.task39.task3913;

import com.javarush.task.task39.task3913.query.*;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class LogParser implements IPQuery, UserQuery, DateQuery, EventQuery, QLQuery {
    private final Path logDir;
    private List<LogRecord> logRecords = new ArrayList<>();

    public LogParser(Path logDir) {
        this.logDir = logDir;
        logFromFileReader();
    }

    private void logFromFileReader() {
        DirectoryStream<Path> files;
        try {
            files = Files.newDirectoryStream(logDir);
            for (Path file : files) {
                if (Files.isRegularFile(file) && file.getFileName().toString().endsWith(".log")) {
                    List<String> list = Files.readAllLines(file);
                    parseLogFile(list);
                }
            }

        } catch (IOException | ParseException e) {
            System.out.println(e);
        }
    }

    private void parseLogFile(List<String> list) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.ENGLISH);
        for (String parameters : list) {
            String[] params = parameters.split("\\t");
            if (params.length > 5)
                continue;
            String ip = params[0];
            String userName = params[1];
            Date date = dateFormat.parse(params[2]);
            String[] statusEvent = params[3].split(" ");
            Event event = Event.valueOf(statusEvent[0]);
            Integer taskNum = null;
            if (statusEvent.length > 1)
                taskNum = Integer.valueOf(statusEvent[1]);
            Status status = Status.valueOf(params[4]);
            logRecords.add(new LogRecord(ip, userName, date, event, taskNum, status));
        }
    }

    @Override
    public int getNumberOfUniqueIPs(Date after, Date before) {
        return getUniqueIPs(after, before).size();
    }

    @Override
    public Set<String> getUniqueIPs(Date after, Date before) {
        HashSet<String> uniqueIps = new HashSet<>();
        for (LogRecord rec : logRecords) {
            if ((after == null || rec.date.getTime() >= after.getTime()) &&
                    (before == null || rec.date.getTime() <= before.getTime())) {
                uniqueIps.add(rec.ip);
            }
        }
        return uniqueIps;
    }

    @Override
    public Set<String> getIPsForUser(String user, Date after, Date before) {
        HashSet<String> userIPs = new HashSet<>();
        for (LogRecord rec : logRecords) {
            if (rec.user.equals(user) && (after == null || rec.date.getTime() >= after.getTime()) &&
                    (before == null || rec.date.getTime() <= before.getTime())) {
                userIPs.add(rec.ip);
            }
        }
        return userIPs;
    }

    @Override
    public Set<String> getIPsForEvent(Event event, Date after, Date before) {
        HashSet<String> eventIPs = new HashSet<>();
        for (LogRecord rec : logRecords) {
            if (rec.event.equals(event) && (after == null || rec.date.getTime() >= after.getTime()) &&
                    (before == null || rec.date.getTime() <= before.getTime())) {
                eventIPs.add(rec.ip);
            }
        }
        return eventIPs;
    }

    @Override
    public Set<String> getIPsForStatus(Status status, Date after, Date before) {
        HashSet<String> statusIPs = new HashSet<>();
        for (LogRecord rec : logRecords) {
            if (rec.eventStatus.equals(status) && (after == null || rec.date.getTime() >= after.getTime()) &&
                    (before == null || rec.date.getTime() <= before.getTime())) {
                statusIPs.add(rec.ip);
            }
        }
        return statusIPs;
    }

    @Override
    public Set<String> getAllUsers() {
        return logRecords.stream()
                .map(LogRecord::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public int getNumberOfUsers(Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .map(LogRecord::getUser)
                .collect(Collectors.toSet())
                .size();

    }

    @Override
    public int getNumberOfUserEvents(String user, Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec -> rec.getUser().equals(user))
                .map(LogRecord::getEvent)
                .collect(Collectors.toSet())
                .size();
    }

    @Override
    public Set<String> getUsersForIP(String ip, Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec -> rec.getIp().equals(ip))
                .map(LogRecord::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getLoggedUsers(Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec -> rec.getEvent().equals(Event.LOGIN))
                .map(LogRecord::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getDownloadedPluginUsers(Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec -> rec.getEvent().equals(Event.DOWNLOAD_PLUGIN))
                .map(LogRecord::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getWroteMessageUsers(Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec -> rec.getEvent().equals(Event.WRITE_MESSAGE))
                .map(LogRecord::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getSolvedTaskUsers(Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec -> rec.getEvent().equals(Event.SOLVE_TASK))
                .map(LogRecord::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getSolvedTaskUsers(Date after, Date before, int task) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec -> rec.getEvent().equals(Event.SOLVE_TASK) && rec.taskNum == task)
                .map(LogRecord::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getDoneTaskUsers(Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec -> rec.getEvent().equals(Event.DONE_TASK))
                .map(LogRecord::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getDoneTaskUsers(Date after, Date before, int task) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.event.equals(Event.DONE_TASK) && (rec.taskNum == task))
                .map(LogRecord::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Date> getDatesForUserAndEvent(String user, Event event, Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.getUser().equals(user))
                .filter(rec ->
                        rec.getEvent().equals(event))
                .map(LogRecord::getDate)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Date> getDatesWhenSomethingFailed(Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.getEventStatus().equals(Status.FAILED))
                .map(LogRecord::getDate)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Date> getDatesWhenErrorHappened(Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.getEventStatus().equals(Status.ERROR))
                .map(LogRecord::getDate)
                .collect(Collectors.toSet());
    }

    @Override
    public Date getDateWhenUserLoggedFirstTime(String user, Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.getUser().equals(user))
                .filter(rec ->
                        rec.getEvent().equals(Event.LOGIN))
                .map(LogRecord::getDate)
                .sorted()
                .findFirst()
                .orElse(null);
    }

    @Override
    public Date getDateWhenUserSolvedTask(String user, int task, Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.getUser().equals(user))
                .filter(rec ->
                        rec.getEvent().equals(Event.SOLVE_TASK))
                .filter(rec ->
                        rec.getTaskNum().equals(task))
                .map(LogRecord::getDate)
                .sorted()
                .findFirst()
                .orElse(null);
    }

    @Override
    public Date getDateWhenUserDoneTask(String user, int task, Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.getUser().equals(user))
                .filter(rec ->
                        rec.getEvent().equals(Event.DONE_TASK))
                .filter(rec ->
                        rec.getTaskNum().equals(task))
                .map(LogRecord::getDate)
                .sorted()
                .findFirst()
                .orElse(null);
    }

    @Override
    public Set<Date> getDatesWhenUserWroteMessage(String user, Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.getUser().equals(user))
                .filter(rec ->
                        rec.getEvent().equals(Event.WRITE_MESSAGE))
                .map(LogRecord::getDate)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Date> getDatesWhenUserDownloadedPlugin(String user, Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.getUser().equals(user))
                .filter(rec ->
                        rec.getEvent().equals(Event.DOWNLOAD_PLUGIN))
                .map(LogRecord::getDate)
                .collect(Collectors.toSet());
    }

    @Override
    public int getNumberOfAllEvents(Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .map(LogRecord::getEvent)
                .collect(Collectors.toSet())
                .size();
    }

    @Override
    public Set<Event> getAllEvents(Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .map(LogRecord::getEvent)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Event> getEventsForIP(String ip, Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.getIp().equals(ip))
                .map(LogRecord::getEvent)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Event> getEventsForUser(String user, Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.getUser().equals(user))
                .map(LogRecord::getEvent)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Event> getFailedEvents(Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.getEventStatus().equals(Status.FAILED))
                .map(LogRecord::getEvent)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Event> getErrorEvents(Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.getEventStatus().equals(Status.ERROR))
                .map(LogRecord::getEvent)
                .collect(Collectors.toSet());
    }

    @Override
    public int getNumberOfAttemptToSolveTask(int task, Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.getEvent().equals(Event.SOLVE_TASK))
                .filter(rec ->
                        rec.getTaskNum().equals(task))
                .collect(Collectors.toSet())
                .size();
    }

    @Override
    public int getNumberOfSuccessfulAttemptToSolveTask(int task, Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.getEvent().equals(Event.DONE_TASK))
                .filter(rec ->
                        rec.getTaskNum().equals(task))
                .collect(Collectors.toSet())
                .size();
    }

    @Override
    public Map<Integer, Integer> getAllSolvedTasksAndTheirNumber(Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.getEvent().equals(Event.SOLVE_TASK))
                .collect(Collectors.groupingBy(LogRecord::getTaskNum,  Collectors.reducing(0, e -> 1, Integer::sum)));
    }

    @Override
    public Map<Integer, Integer> getAllDoneTasksAndTheirNumber(Date after, Date before) {
        return logRecords.stream()
                .filter(rec ->
                        (after == null || rec.date.getTime() >= after.getTime()) &&
                                (before == null || rec.date.getTime() <= before.getTime()))
                .filter(rec ->
                        rec.getEvent().equals(Event.DONE_TASK))
                .collect(Collectors.groupingBy(LogRecord::getTaskNum,  Collectors.reducing(0, e -> 1, Integer::sum)));
    }

    @Override
    public Set<Object> execute(String query) {

        String shortQuery = query.replace("get ", "");


        switch (shortQuery) {
            case "ip":
            {
                return new HashSet<>(getUniqueIPs(null, null));
            }
            case "user":
            {
                return new HashSet<>(getAllUsers());
            }
            case "date":
            {
                return new HashSet<>(getAllDates());
            }
            case "event":
            {
                return new HashSet<>(getAllEvents(null, null));
            }
            case "status":
                return new HashSet<>(getAllStatus());
        }
        return null;
    }

    private Set<Status> getAllStatus() {
        return logRecords.stream()
                .map(LogRecord::getEventStatus)
                .collect(Collectors.toSet());
    }

    public Set<Date> getAllDates() {
        return logRecords.stream()
                .map(LogRecord::getDate)
                .collect(Collectors.toSet());
    }

    private class LogRecord {
        private String ip;
        private String user;
        private Date date;
        private Event event;
        private Integer taskNum;
        private Status eventStatus;

        public Integer getTaskNum() {
            return taskNum;
        }

        public String getIp() {
            return ip;
        }

        public String getUser() {
            return user;
        }

        public Date getDate() {
            return date;
        }

        public Event getEvent() {
            return event;
        }

        public Status getEventStatus() {
            return eventStatus;
        }

        public LogRecord(String ip, String user, Date date, Event event, Integer taskNum, Status eventStatus) {
            this.ip = ip;
            this.user = user;
            this.date = date;
            this.event = event;
            this.taskNum = taskNum;
            this.eventStatus = eventStatus;
        }
    }
}