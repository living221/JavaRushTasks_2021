package com.javarush.task.task39.task3911;

import java.util.*;

public class Software {
    private int currentVersion;

    private Map<Integer, String> versionHistoryMap = new LinkedHashMap<>();

    public void addNewVersion(int version, String description) {
        if (version > currentVersion) {
            versionHistoryMap.put(version, description);
            currentVersion = version;
        }
    }

    public int getCurrentVersion() {
        return currentVersion;
    }

    public Map<Integer, String> getVersionHistoryMap() {
        return Collections.unmodifiableMap(versionHistoryMap);
    }

    public boolean rollback(int rollbackVersion) {
        if (!versionHistoryMap.containsKey(rollbackVersion)) {
            return false;
        } else {
//            versionHistoryMap.entrySet().removeIf(entries->entries.getKey() > rollbackVersion);
            versionHistoryMap.keySet().removeIf(version->version > rollbackVersion);
//            ArrayList<Integer> toRemove = new ArrayList<>();
//            for (Integer version : versionHistoryMap.keySet()) {
//                if (version > rollbackVersion) {
//                    toRemove.add(version);
//                }
//            }
//            for (Integer versionToRemove : toRemove) {
//                versionHistoryMap.remove(versionToRemove);
//            }
            currentVersion = rollbackVersion;
            return true;
        }
    }
}
