package com.javarush.task.task39.task3908;

/* 
Возможен ли палиндром?
*/

import org.junit.Assert;

import java.util.ArrayList;
import java.util.Locale;

public class Solution {
    public static void main(String[] args) {
        boolean actual = Solution.isPalindromePermutation("abcaacb");
        Assert.assertTrue(actual);
        actual = Solution.isPalindromePermutation("abcaacbabcaacb");
        Assert.assertTrue(actual);
        actual = Solution.isPalindromePermutation("aaavvv");
        Assert.assertFalse(actual);
    }

    public static boolean isPalindromePermutation(String s) {
        String lowerCaseString = s.toLowerCase(Locale.ROOT);
        byte[] letters = lowerCaseString.getBytes();
        ArrayList<Byte> bytes = new ArrayList<>();
        for (byte b : letters) {
            bytes.add(b);
        }
        int iterations = bytes.size()/2;
        int pairFirst = 0;
        int pairSecond = 0;
        for (int k = 0; k < iterations; k++) {
            boolean pairFound = false;
            for (int i = 0; i < bytes.size(); i++) {
                if (pairFound) {
                    bytes.remove(pairSecond);
                    bytes.remove(pairFirst);
                    break;
                }
                for (int j = i + 1; j < bytes.size(); j++) {
                    if (bytes.get(i).equals(bytes.get(j))) {
                        pairFirst = i;
                        pairSecond = j;
                        pairFound = true;
                        break;
                    }
                }
            }
        }
        return bytes.size() <= 1;
    }

    public static boolean isPalindromePermutation2(String s) {
        boolean foundOdd = false;
        int[] tableCount = new int[256];

        for (char c : s.toLowerCase().toCharArray()) {
            tableCount[c] += 1;
        }

        for (int count : tableCount) {
            if (count % 2 != 0) {
                if (foundOdd) {
                    return false;
                }
                foundOdd = true;
            }
        }

        return true;
    }
}
